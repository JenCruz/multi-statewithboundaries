#!/usr/bin/env python

"""
Created by Dr Jennyffer Cruz

Basic Functions include:

logit()
inv_logit()
lognormpdf()
beta_a()
beta_b()
beta_mean()
beta_var()
"""

#importing required modules
import numpy as np
from scipy import stats
from scipy.stats.mstats import mquantiles

#defining global functions 
def logit(x):
    """
    Function to convert probability to real number
    """
    return np.log( x / ( 1 - x ) )


def logit_se( mean, se):
    '''
    Function provides standard error in logit scale
    if mean and standard error in untransformed scale
    are provided
    '''
    return se / ( mean * ( 1 - mean ) )


def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))

def lognormpdf( x, mu, S ):
    """ 
    Function to get log multivariate normal distribution function 
    where x = bgibbs, mu = bprior, S = vmat (correlation structure)
    Calculate gaussian probability density of x, when x ~ N(mu,sigma) 
    """
    nx = len(S)
    tmp = -0.5 * (nx * np.log( 2 * np.pi ) + np.linalg.slogdet(S)[1] )
    err = x - mu
    numerator = np.linalg.solve(S, err).T.dot(err)
    return tmp - numerator

def quantiles(a):
    '''
    Computes 95% quantiles
    '''
    return stats.mstats.mquantiles(a, prob=[0.025, 0.5, 0.975])

def beta_a( mean, var ):
    """
    Function to calculate a parameter of a beta function
    from mean and variance values
    """
    return mean * ( ( mean * ( 1.0 - mean ) ) / var - 1.0 ) 
             
def beta_b( mean, var ):
    """
    Function to calculate b parameter of a beta function
    from mean and a values
    """
    return ( 1.0 - mean ) * ( ( mean * ( 1.0 - mean ) ) / var - 1.0 ) 

def beta_mean( a, b ):
    """
    Function to calculate mean from  a & b parameters
    of a beta distribution
    """
    return ( a * ( a + 1 ) ) / ( ( a + b + 1 ) * ( a + b ) ) 

def beta_var( a, b ):
    """
    Function to calculate variance from  a & b parameters
    of a beta distribution
    """
    return ( a* b ) / ( ( (a + b )**2 ) * ( a + b + 1 ) ) 

def beta_ab( mean, var):
    '''
    Dean's version of getting a&b parameteres 
    for the beta distribution
    '''
    a = mean * ( ( mean * ( 1.0 - mean ) ) / var - 1.0 )
    b = ( 1.0 - mean ) * ( ( mean * ( 1.0 - mean ) ) / var - 1.0 )
    ab = np.array( [a,b] )
    return ab
    
def gamma_shp( mean, scale): 
    '''
    Function to calculate shape of a gamma 
    distribution when mean and scale are known
    '''
    return ( mean / scale ) 


if __name__ == '__main__':
    main()

######## end of script    ###############