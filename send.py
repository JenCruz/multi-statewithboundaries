#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
"""
Created by Dr Jennyffer Cruz
This script is designed to send jobs to the Nesi (NZ escience 
infrastructure high-performance computing facilities)
by calling relevant python scripts and pickles (if mcmc run is re-started)
"""
#importing required python scripts
import mcmc #script that runs mcmc
import optparse
from submit import PerfTest

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--runparsfile", dest="runparsfile", help="Input pickled runpars file")
        p.add_option("--resultsfile", dest="resultsfile", help="Input pickled results file")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)
                                    

cmdargs = CmdArgs()
print(cmdargs.runparsfile)
print(cmdargs.resultsfile)


mcmc.main( pklresults = cmdargs.resultsfile, pklrunpars = cmdargs.runparsfile ) 
           
                
######## end of script ####################