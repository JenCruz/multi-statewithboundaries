#!/usr/bin/env python

'''

Created by Dr Jennyffer Cruz
this script simulates data for a multi-state, multi-season occupancy model  
The model has two submodels: 1) an observation submodel
that combines expert opinion with repeated survey data, and  
2) a biological process submodel that links environmental covariates to 
transition probability parameters.
The abundance states are = low, medium and high.

'''

#import required modules
import numpy as np
from scipy import stats
import basic
import pandas as pd
import os
import pickle
#import submit
from submit import PerfTest

class SetScenario( object ):
    '''
    This class gets the scenarioID (scenario number) to be run in parallel 
    processing, assesing the performance of the model
    The class also unpickles the table that was created in 
    submit.py, which contains parameter values to be tested
    in model performance assessment
    '''
    def __init__( self ) :

        jobName = os.getenv('SLURM_JOB_NAME')
##        print(jobName)

        scenarioID, trialNum = jobName.split('_')
        self.scenarioID = int( scenarioID )
        self.trialNum = int( trialNum )

        outpath = os.getenv('MICE') #replace 'MICE' with location of relevant project folder
        outperfpars = os.path.join( outpath, 'perfpars.pkl' )
        fileobj = open(outperfpars, 'rb' )
        self.perfpars = pickle.load( fileobj )
        fileobj.close()
    
#simulate data
class SimData( object ) :
    '''
    Simulated mouse dynamics data.
    Process submodel:
    includes two logit functions that link
    rainfall to parameters in transition prob matrix: tau_lh and psi_l  
    Observation submodel: 
    Uses informed priors to define boundary distributions that
    partition the data into latent abundance states    
    '''
    def __init__( self ) :
        
        setscen = SetScenario()
        self.perfpars = setscen.perfpars
        self.scenarioID = setscen.scenarioID
        self.trialNum = setscen.trialNum
        
        #define extent of data:
        self.sites = 100 #number of sites sampled
        self.T = 16 #number of primary seasons 
        self.k = int( self.perfpars.table[ self.scenarioID, 0 ] ) #number of repeat surveys within a season
        self.q = 2 #number of monitoring methods
        self.nid = np.arange( (self.sites * self.T), dtype= np.int32 ) #id for each abundance based on site and season (only 1 per site per season)       
        self.ztrap_nid = np.repeat( self.nid, self.k ) #id for each survey based on site and season (multiple surveys possible per site per season so repeat z_nid values)
        self.zchew_nid = np.repeat( self.nid, self.k )
        self.ztrapid = np.arange( self.ztrap_nid.shape[0] ) #id for each survey for all sites and seasons
        self.zchewid = np.arange( self.zchew_nid.shape[0] )
        
        ###### Biological process model  ########
        
        #define 'actual' abundance states:
                 
        #inital prob of being in a given abundance state
        phi0_l =0.7
        phi0_m = 0.2
        phi0_h = 0.1
        phi_t0 = np.random.multinomial( n = self.sites, pvals = [phi0_l, 
            phi0_m, phi0_m] )
        #initial abundance states (at time = 0 ):
        N0 = np.zeros(self.sites, dtype = np.int8 )
        N0[ : phi_t0[ 0 ] ] = 0 # low
        N0[ phi_t0[ 0 ] : np.sum( phi_t0[ :2 ] ) ] = 1 # med abundance state
        N0[ np.sum( phi_t0[ :2 ] ): ] = 2 #high abundance state
        
        #transition probability models (link with environmental covariates):
        
        b0 = -3 #  #intercept for taul_lh transition probs
        b1 = 3 # coefficient for rain for tau_hl function
        a0 = 2 #int for psi_l function
        a1 = -3 #coef for rain in psi_l function
        self.beta_lh = np.array( [b0, b1] )
        self.alpha_l = np.array( [a0, a1] )
        #rainfall values are total rainfall range for crop-growing region in VIC & SA for 2011-2013
        self.rain =  np.random.uniform( low = 0.00, high = 150.0, size = self.sites*
            (self.T-1) ) #rainfall covariate (x1) excluding the last season
        #standardising rain following Gelman(2008):
        mean_rain = np.mean(self.rain)
        sd_rain = np.std(self.rain)
        stnd_rain = ( self.rain - mean_rain ) / ( 2 * sd_rain )
        x0 = np.ones( self.sites * (self.T-1) ) #intercept
        x = np.vstack( [x0, stnd_rain] ) #covariate
        self.x = np.transpose(x)
        self.logit_tau_lh = np.dot(self.x, self.beta_lh) #logit of transition from low to high 
        self.logit_psi_l = np.dot(self.x, self.alpha_l) #logit of med-high transition
        self.tau_lh = basic.inv_logit(self.logit_tau_lh) #prob of transitioning from low to high (mouse plague)
        self.psi_l = basic.inv_logit(self.logit_psi_l) #prob of remaining in low abundance state
        self.tau_hl =0.7  #transition prob high-low (population crash)
        self.tau_mh =0.2 #transition prob med-high
        self.psi_m =0.1 #prob of staying in med
        self.psi_h =0.1 #prob of staying in high
        
        #simulating transitions from initial state for each site across time
        self.N_it = np.zeros( self.sites * self.T, dtype = np.int8 )
        self.N_it[:self.sites] = N0 #initial abundance state
        self.true_state()
        
        #### observation submodel  ######
        
        #trapping data        
        mu_lm_trap = 0.33 #cut off between low and medium
        mu_mh_trap = 0.66 #cut off between medium and high
        #mean cutoff distribution for trapping
        self.mu_trap = np.hstack( [ self.perfpars.table[self.scenarioID,3], self.perfpars.table[self.scenarioID,4] ] )# np.array( [mu_lm_trap, mu_mh_trap ] )#
        logit_mu_trap = basic.logit(self.mu_trap) #transform means to logit scale
        self.sig_trap = np.hstack( [ self.perfpars.table[self.scenarioID,1], self.perfpars.table[self.scenarioID,1] ] )# np.array( [ 0.02, 0.02 ] ) #
        self.z_trap = self.gen_data( self.mu_trap, self.sig_trap ) #determine observed states

        #chew card data
        mu_lm_chew= 0.33 #cut off between low and medium
        mu_mh_chew = 0.66 #cut off between medium and high
        self.mu_chew = np.hstack( [ self.perfpars.table[self.scenarioID,5], self.perfpars.table[self.scenarioID,6] ] )#np.array( [mu_lm_chew, mu_mh_chew ] ) #
        logit_mu_chew = basic.logit ( self.mu_chew )
        self.sig_chew = np.hstack( [ self.perfpars.table[self.scenarioID,2], self.perfpars.table[self.scenarioID,2] ] ) #np.array( [ 0.02, 0.02 ] )#
        self.z_chew = self.gen_data( self.mu_chew, self.sig_chew ) #determine observed states

        #loop for removing a proportion of the data to reflect some sites not getting sampled on some seasons
        self.remove = self.perfpars.table[self.scenarioID,11] #0 #percentage of data to randomly remove
        self.NAtrap = np.zeros( self.nid.shape )
        self.NAchew = np.zeros( self.nid.shape )
        
        if self.remove > 0:
            self.NAtrap = np.ones( self.nid.shape )
            self.NAchew = np.ones( self.nid.shape )
            keep = int( self.nid.shape[0] * ( (100 - self.remove) / 100 )  )
            nindex = np.sort( np.random.choice( self.nid , keep, replace = False ) )

            self.z_trap = self.z_trap[ np.in1d(self.ztrap_nid, nindex) ]
            self.z_chew = self.z_chew[ np.in1d(self.zchew_nid,nindex) ]
        
            self.ztrap_nid = self.ztrap_nid[ np.in1d(self.ztrap_nid, nindex ) ]
            self.zchew_nid = self.zchew_nid[ np.in1d(self.zchew_nid, nindex ) ] 

            self.ztrapid = np.arange( self.ztrap_nid.shape[0] )
            self.zchewid = np.arange( self.zchew_nid.shape[0] )

            self.NAtrap[ nindex ] = 0
            self.NAchew[ nindex ] = 0
            
    def gen_data(self, mu, sig ):
        '''               
        This function generates observed data given 
        true abundance states and true boundary distributions
        A multinomial draw is then used to derive data which are
        transformed back into real scale (between 0 and 1)
        attributes:
        z = monitoring data to be created
        mu = mean of normal cutoff distributions on real scale
        sig = error of normal cutoff distributions on logit scale
        N_it = is the true abundance at each site for each season
        '''
        logitmu = basic.logit(mu)
        z = np.zeros( ( (self.sites * self.T), self.k ) ) #data vector
        id0 = np.where( self.N_it == 0 )
        id1 = np.where( self.N_it == 1 )
        id2 = np.where( self.N_it == 2 )
        pseudo = np.linspace( 0.01, 0.99, 1000 ) #pseudo data in a real scale from 0 to 1
        
        def get_prPseudo(pseudo, logitmu, sig):
            '''
            This function uses pseudo data in logit scale to create
            probability of pseudo data given true N = 0,1,2 
            and the true boundary distribution values (mu and sig)
            attributes:
            pseudo =  the pseudo data in logit scale
            logitmu = means of two boundary distributions on logit scale
            sig = std devs of two boundary distribs on logit scale
            '''
            logitpseudo = basic.logit(pseudo)
            cdf01 = stats.norm.cdf( logitpseudo, logitmu[0], sig[0] )
            cdf12 = stats.norm.cdf( logitpseudo, logitmu[1], sig[1] ) 
            prZN0 =  ( 1 - cdf01 ) * ( 1 - cdf12 ) #prob of data given N = 0
            prZN1 = cdf01 * ( 1 - cdf12 ) #prob of data given N = 1
            prZN2 = cdf12 * cdf01 #prob of data given N = 2
            pvals = np.vstack( [prZN0, prZN1, prZN2] )
#            pvals = np.transpose( pvals ) #2d array of prob of data given N = 0, 1, 2. Size= [500,3] 
            #making combined probabilities sum to 1:
            sumpvals = np.sum( pvals, axis = 1 ) 
            prPseudo = pvals / sumpvals[:,None] #adj combined probs, with rows summing to 1
            return prPseudo

        prPseudo = get_prPseudo(pseudo, logitmu, sig)

        pseudo0 = np.zeros( len(z[id0]) )
        pseudo1 = np.zeros( len(z[id1]) )
        pseudo2 = np.zeros( len(z[id2]) )
        #loop over each survey
        for k in range(self.k):
            for i in range( len(pseudo0) ):     
                pseudo0[i] = pseudo[ np.where( np.random.multinomial( n = 1, pvals =  prPseudo[0] ) ) ]
            z[ id0,k ] = pseudo0    

            for i in range( len(pseudo1) ):     
                pseudo1[i] = pseudo[ np.where( np.random.multinomial( n = 1, pvals =  prPseudo[1] ) ) ]
            z[ id1, k ] = pseudo1    
            for i in range( len(pseudo2) ):     
                pseudo2[i] = pseudo[ np.where( np.random.multinomial( n = 1, pvals =  prPseudo[2] ) ) ]
            z[ id2, k ] = pseudo2    

        return z.flatten()#aggregate results of all surveys into the same column (as IDarray)
        
    def true_state(self):
        '''
        This function predicts true states for t>0
        '''    
        for t in range( self.T - 1 ):
            for i in range( self.sites ):
                idx = ( t * self.sites ) + i
                idx2 = ( ( t + 1 ) * self.sites ) + i
                if self.N_it[idx] == 0:
                    TPM = np.array( [ self.psi_l[idx], ( 1 - self.psi_l[idx] ) * (1 - 
                        self.tau_lh[idx]), ( 1 - self.psi_l[idx] ) * self.tau_lh[idx]  ])
                    phi = np.random.multinomial( 1, TPM )
                    if phi[0] == 1:
                        self.N_it[idx2] = 0
                    elif phi[1] == 1:
                        self.N_it[idx2] = 1
                    else:
                        self.N_it[idx2] = 2
                elif self.N_it[idx] == 1:
                    TPM = np.array( [ ( 1 - self.psi_m ) * ( 1 - self.tau_mh
                        ), self.psi_m, ( 1 - self.psi_m ) * self.tau_mh ] )
                    phi = np.random.multinomial( 1, TPM )
                    if phi[0] == 1:
                        self.N_it[idx2] = 0
                    elif phi[1] == 1:
                        self.N_it[idx2] = 1
                    else:
                        self.N_it[idx2] = 2
                else:
                    TPM = np.array( [ ( 1 - self.psi_h ) * self.tau_hl, 
                        ( 1 - self.psi_h ) * ( 1 - self.tau_hl ), self.psi_h ] )
                    phi = np.random.multinomial( 1, TPM )
                    if phi[0] == 1:
                        self.N_it[idx2] = 0
                    elif phi[1] == 1:
                        self.N_it[idx2] = 1
                    else:
                        self.N_it[idx2] = 2
                        
########            Main function
#######
def main():

    sim_data = SimData()
    setscen = SetScenario()
    
#retrieving pickled table of parameter values for each scenario:
    if os.path.exists( pklperfpars ):
        fileobj = open( pklperfpars, 'rb' )
        perfpars = pickle.load( fileobj )
        fileobj.close()
    
    jobName = os.getenv('SLURM_JOB_NAME')
    scenarioID, trialNum = jobName.split('_')
    scenarioID = int(scenarioID)

    
if __name__ == '__main__':
        main()

###################### end of script   #################