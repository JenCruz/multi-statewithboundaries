#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
"""
Created on 13 Feb 2014 by Jen Cruz
Modified on 5 Nov 2014 by Jen Cruz
This file contains options for viewing results from MCMC
It brings in results pickled created in  mcmc.py
"""

#importing required modules and python files
#import os
import numpy as np
#from scipy.stats.mstats import mquantiles
import pickle
import prettytable
import mcmc
import basic
import optparse
import pylab as P
from scipy import stats
#import matplotlib.pyplot as P

 
class PerfResults( object ):
    '''
    Saves mean, median and 95% CIs for each parameter based on overall MCMC run
    '''
#    def __init__( self, results, runpars ):
    def __init__( self, pklresults, pklrunpars ):
        pklfile = open( pklresults, 'rb' )
        results = pickle.load( pklfile )
        pklfile.close()

        fileobj = open(pklrunpars,'rb') #rb because you open it for reading it
        runpars = pickle.load(fileobj)
        fileobj.close()
        
        pars = 16 #number of parameters of interest
        self.means = np.zeros( pars )
        self.coverage = np.zeros( pars )
        self.scenarioID = runpars.data.scenarioID
        self.trialNum = 2 #runpars.data.trialNum        

        def PostMeans():
            self.means[ 0:2 ] = np.apply_along_axis( np.mean, 0, results.a_gibbs )
            self.means[ 2:4 ] = np.apply_along_axis( np.mean, 0, results.b_gibbs )
            self.means[ 4:6 ] = np.apply_along_axis( np.mean, 0, results.tau_gibbs[:,1:] )
            self.means[ 6:8 ] = np.apply_along_axis( np.mean, 0, results.psi_gibbs[:,1:] )
            self.means[ 8:10 ] = np.apply_along_axis( np.mean, 0, results.mu_trap_gibbs )
            self.means[ 10:12 ] = np.apply_along_axis( np.mean, 0, results.sig_trap_gibbs )
            self.means[ 12:14 ] = np.apply_along_axis( np.mean, 0, results.mu_chew_gibbs )
            self.means[ 14:16 ] = np.apply_along_axis( np.mean, 0, results.sig_chew_gibbs )
            self.means = np.round( self.means, decimals = 3 )

        def Coverage():
            self.resultTable = np.zeros( shape = (3, pars) )
            self.resultTable[ 0:3, 0:2 ] = np.apply_along_axis( basic.quantiles, 0, results.a_gibbs )
            self.resultTable[ 0:3, 2:4 ] = np.apply_along_axis( basic.quantiles, 0, results.b_gibbs )
            self.resultTable[ 0:3, 4:6 ] = np.apply_along_axis( basic.quantiles, 0, results.tau_gibbs[:,1:] )
            self.resultTable[ 0:3, 6:8 ] = np.apply_along_axis( basic.quantiles, 0, results.psi_gibbs[:,1:] )
            self.resultTable[ 0:3, 8:10 ] = np.apply_along_axis( basic.quantiles, 0, results.mu_trap_gibbs )
            self.resultTable[ 0:3, 10:12 ] = np.apply_along_axis( basic.quantiles, 0, results.sig_trap_gibbs )
            self.resultTable[ 0:3, 12:14 ] = np.apply_along_axis( basic.quantiles, 0, results.mu_chew_gibbs )
            self.resultTable[ 0:3, 14:16 ] = np.apply_along_axis( basic.quantiles, 0, results.sig_chew_gibbs )
       
            alpha =  runpars.data.alpha_l 
            beta = runpars.data.beta_lh 
            tau_mh = runpars.data.tau_mh
            tau_hl = runpars.data.tau_hl 
            psi_m = runpars.data.psi_m 
            psi_h = runpars.data.psi_h 
            mu_trap = runpars.data.mu_trap
            mu_chew = runpars.data.mu_chew
            sig_trap = runpars.data.sig_trap
            sig_chew = runpars.data.sig_chew 
       
            self.real = np.hstack( [ alpha, beta, tau_mh, tau_hl, psi_m, psi_h, mu_trap, sig_trap, mu_chew, sig_chew ] )
            for i in range( pars ):
                if self.real[i] >= self.resultTable[0,i] and self.real[i] <= self.resultTable[2,i]:
                    self.coverage[i] = 1

        PostMeans()
        print('means', self.means)
        Coverage()
        print( 'coverage', self.coverage )
        print( 'resultTable',self.resultTable )

class Postprocess(object):
    '''
    This class creates a summay table from results pickled after an 
    MCMC run (from mcmc.py script)
    '''
#    def __init__(self, results):
#        self.results = results 
#        self.runpars = runpars 
        
    def __init__( self, pklresults, pklrunpars ):
        pklfile = open( pklresults, 'rb' )
        self.results = pickle.load( pklfile )
        pklfile.close()

        fileobj = open(pklrunpars,'rb') #rb because you open it for reading it
        self.runpars = pickle.load(fileobj)
        fileobj.close()

    
#    def summTable(self, tablefile = None):
    def summTable(self):
        #quantiles:
        resultTable = np.zeros( shape = (4,16) )
        resultTable[ 0:3, 0:2 ] = np.apply_along_axis(basic.quantiles, 0, self.results.a_gibbs)
        resultTable[ 0:3, 2:4 ] = np.apply_along_axis(basic.quantiles, 0, self.results.b_gibbs)
        resultTable[ 0:3, 4:6 ] = np.apply_along_axis(basic.quantiles, 0, self.results.tau_gibbs[:,1:])
        resultTable[ 0:3, 6:8 ] = np.apply_along_axis(basic.quantiles, 0, self.results.psi_gibbs[:,1:])
        resultTable[ 0:3, 8:10 ] = np.apply_along_axis(basic.quantiles, 0, self.results.mu_trap_gibbs)
        resultTable[ 0:3, 10:12 ] = np.apply_along_axis(basic.quantiles, 0, self.results.sig_trap_gibbs)
        resultTable[ 0:3, 12:14 ] = np.apply_along_axis(basic.quantiles, 0, self.results.mu_chew_gibbs)
        resultTable[ 0:3, 14:16 ] = np.apply_along_axis(basic.quantiles, 0, self.results.sig_chew_gibbs)
        #means:       
        resultTable[ -1, 0:2 ] = np.apply_along_axis(np.mean, 0, self.results.a_gibbs)
        resultTable[ -1, 2:4 ] = np.apply_along_axis(np.mean, 0, self.results.b_gibbs)
        resultTable[ -1, 4:6 ] = np.apply_along_axis(np.mean, 0, self.results.tau_gibbs[:,1:])
        resultTable[ -1, 6:8 ] = np.apply_along_axis(np.mean, 0, self.results.psi_gibbs[:,1:])
        resultTable[ -1, 8:10 ] = np.apply_along_axis(np.mean, 0, self.results.mu_trap_gibbs)
        resultTable[ -1, 10:12 ] = np.apply_along_axis(np.mean, 0, self.results.sig_trap_gibbs)
        resultTable[ -1, 12:14 ] = np.apply_along_axis(np.mean, 0, self.results.mu_chew_gibbs)
        resultTable[ -1, 14:16 ] = np.apply_along_axis(np.mean, 0, self.results.sig_chew_gibbs)
       
        resultTable = np.round(resultTable, decimals = 3 )
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])
        
        names = [ 'a0', 'a1','b0', 'b1', 'tau_mh', 'tau_hl', 'psi_m', 'psi_h',
                 'mu0_trap', 'mu1_trap', 'sig0_trap', 'sig1_trap','mu0_chew', 'mu1_chew', 'sig0_chew', 'sig1_chew' ]
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
#        if tablefile is not None: #this creates a file of sumtable if filename given
#            fileobj = open(tablefile,'w')
#            fileobj.write(aa)
#            fileobj.close

    def SummStats( self ):
        '''
        some summary values
        '''
        print( 'real parameters:')
        print( 'sites', self.runpars.data.sites, 'T', self.runpars.data.T, 'k', self.runpars.data.k )
        print( 'real alpha:', self.runpars.data.alpha_l )
        print( 'real beta:', self.runpars.data.beta_lh )
        print( 'real tau_mh:', self.runpars.data.tau_mh, 'real tau_hl:', self.runpars.data.tau_hl )
        print( 'real psi_m;', self.runpars.data.psi_m, 'real psi_h:', self.runpars.data.psi_h )

        print('real mu_trap:', self.runpars.data.mu_trap, 'real mu_chew:', self.runpars.data.mu_chew )
        print('real sig_trap:', self.runpars.data.sig_trap, 'real sig_chew:', self.runpars.data.sig_chew )
        
        print( 'prior distributions:' )
        print( 'vprior', self.runpars.priors.vprior )
        print('alpha normal prior', self.runpars.priors.aprior )
        print('beta normal prior', self.runpars.priors.bprior )
        print( 'tau beta priors', self.runpars.priors.tau_prior[1:,] )
        print( 'psi beta priors', self.runpars.priors.psi_prior[1:,] )
        print( 'mu_trap normal priors', self.runpars.priors.logit_muprior_trap )
        print( 'mu_chew normal priors', self.runpars.priors.logit_muprior_chew )
        print('sig_trap gamma priors', self.runpars.priors.gammprior_trap )
        print('sig_chew gamma priors', self.runpars.priors.gammprior_chew )
        
        print( 'mcmc values:')
        print( 'ngibbs', self.runpars.mcmcpars.ngibbs, 'thin:', self.runpars.mcmcpars.thin, 'burnin', self.runpars.mcmcpars.burnin ) 
        
#        print( 'psi gibbs', self.results.psi_gibbs[-50:,1:3] )
#        print( 'tau gibbs', self.results.tau_gibbs[-50:,1:3] )
    def Ncomps( self ):
        '''
        Comparing estimated N against real N and 
        N assigned from average of indices to real N    
        '''
        realN = self.runpars.data.N_it 
        estN = stats.mode(self.results.Nit_gibbs, axis=0)[0] 
        dif =  estN[0,:] - realN
        nid = self.runpars.nid
        ztrap = self.runpars.data.z_trap
        zchew = self.runpars.data.z_chew
        trap_nid = self.runpars.data.ztrap_nid
        chew_nid = self.runpars.data.zchew_nid
        meanTrap = np.zeros( len(realN) )
        meanChew = np.zeros( len(realN) )
        prior_muTrap = self.runpars.priors.Mprior_trap
        prior_muChew = self.runpars.priors.Mprior_chew
        print( 'totalN', len(nid) )
        Ntrap = np.zeros( len(realN) )
        Nchew = np.zeros( len(realN) )
        
        for i in nid:
            meanTrap[i] = np.mean( ztrap[trap_nid == i] )
            meanChew[i] = np.mean( zchew[chew_nid == i] )
            if meanTrap[i] < prior_muTrap[0]:
                Ntrap[i] = 0
            elif meanTrap[i] > prior_muTrap[1]:
                Ntrap[i] = 2
            elif meanTrap[i] > prior_muTrap[0] and meanTrap[i] < prior_muTrap[1]:
                Ntrap[i] = 1
            else:
                Ntrap[i] = np.nan
            if meanChew[i] < prior_muChew[0]:
                Nchew[i] = 0
            elif meanChew[i] > prior_muChew[1]:
                Nchew[i] = 2
            elif meanChew[i] > prior_muChew[0] and meanChew[i] < prior_muChew[1]:
                Nchew[i] = 1
            else:
                Nchew[i] = np.nan    
                    
            if i > 1500:
                print( 'i', i )    
                print( 'ztrap', ztrap[trap_nid == i] )
                print( 'meantrap', meanTrap[i] )
                print( 'prior_mutrap', prior_muTrap )
                print( 'Ntrap', Ntrap[i] )
        
        trapdif = Ntrap - realN
        chewdif = Nchew - realN
                                
        print( 'ztrap shape', self.runpars.data.z_trap.shape )
        print( 'meantrap shape', meanTrap.shape )        
        print('real N:', self.runpars.data.N_it[-200:] )
        print( 'estimated N', estN[0,-200:], 'shape', estN.shape )
        print('dif between estimated & real N:', dif[-200:] )     
        print('dif between trapN & real N:', trapdif[-200:] )     
        print('dif between chewN & real N:', chewdif[-200:] )     

        print( 'total Nit', len(realN) )
        print( 'total ztrap', len(ztrap) )
        print( 'count of dif>0', np.count_nonzero(dif) )
        print( 'count of trapdif>0', np.count_nonzero(trapdif) )
        print( 'count of chewdif>0', np.count_nonzero(chewdif) )
        

    def SummZdata( self ):
        '''
        summary of simulated data including missclassifications
        '''
        
        ztrap_N_it0 = self.runpars.data.z_trap[self.runpars.data.N_it == 0] 
        ztrap_N_it1 = self.runpars.data.z_trap[self.runpars.data.N_it == 1] 
        ztrap_N_it2 = self.runpars.data.z_trap[self.runpars.data.N_it == 2] 
        zchew_N_it0 = self.runpars.data.z_chew[self.runpars.data.N_it == 0] 
        zchew_N_it1 = self.runpars.data.z_chew[self.runpars.data.N_it == 1] 
        zchew_N_it2 = self.runpars.data.z_chew[self.runpars.data.N_it == 2] 

        print('real trap data when realN = 0', ztrap_N_it0, 'total', ztrap_N_it0.shape )
        print('real trap data when realN = 1', ztrap_N_it1, 'total', ztrap_N_it1.shape  )
        print('real trap data when realN = 2', ztrap_N_it2, 'total', ztrap_N_it2.shape  )
        print('real chewcard data when realN = 0:', zchew_N_it0, 'total', zchew_N_it0.shape  )
        print('real chewcard data when realN = 1', zchew_N_it1, 'total', zchew_N_it1.shape  )
        print('real chewcard data when realN = 2', zchew_N_it2, 'total', zchew_N_it2.shape  )
        

        print('ztrap_N0 over', ztrap_N_it0[np.where(ztrap_N_it0 > self.runpars.data.mu_trap[0] ) ] )
        print('ztrap_N1 under', ztrap_N_it1[np.where(ztrap_N_it1 < self.runpars.data.mu_trap[0] ) ] )
        print('ztrap_N1 over', ztrap_N_it1[np.where(ztrap_N_it1 > self.runpars.data.mu_trap[1] ) ] )
        print('ztrap_N2 under', ztrap_N_it2[np.where(ztrap_N_it2 < self.runpars.data.mu_trap[1] ) ] )
        print('zchew_N0 over', zchew_N_it0[np.where(zchew_N_it0 > self.runpars.data.mu_chew[0] ) ] )
        print('zchew_N1 under', zchew_N_it1[np.where(zchew_N_it1 < self.runpars.data.mu_chew[0] ) ] )
        print('zchew_N1 over', zchew_N_it1[np.where(zchew_N_it1 > self.runpars.data.mu_chew[1] ) ] )
        print('zchew_N2 under', zchew_N_it2[np.where(zchew_N_it2 < self.runpars.data.mu_chew[1] ) ] )


                                                    
    def param_distb_plots(self):
        '''
        plotting prior distributions
        '''
        #normal priors for alpha and beta parameters of psi_l and tau_lh
        fig0 = P.figure(0, figsize = ( 10, 5 ) )
        ax1 = fig0.add_subplot( 221 )
        ax1.set_xlabel( 'alpha parameters' )
        ax1.set_ylabel( 'normal prior density' )
        ax1.set_title( "" )
        range = np.linspace(-15,15,1000)
        mean0 =  self.runpars.priors.aprior[0]
        std0 =  self.runpars.priors.vprior[0,0]
        P.plot( range, stats.norm.pdf(range, loc = mean0, scale= std0 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
        postmean0 =  np.mean( self.results.a_gibbs[:,0])
        poststd0 =  np.std( self.results.a_gibbs[:,0])
        P.plot( range, stats.norm.pdf(range, loc = postmean0, scale= poststd0 ), 
            color='purple', linestyle='-', linewidth=2, label='posterior' )
        P.axvline(self.runpars.data.alpha_l[0], color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        
        ax2 = fig0.add_subplot( 222 )
        ax2.set_xlabel('alpha parameters')
        ax2.set_ylabel('normal prior density')
        ax2.set_title("")
        mean1 = self.runpars.priors.aprior[1]
        std1 =  self.runpars.priors.vprior[1,1]
        P.plot( range, stats.norm.pdf(range, mean1, std1 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
        postmean1 =  np.mean( self.results.a_gibbs[:,1])
        poststd1 =  np.std( self.results.a_gibbs[:,1])
        P.plot( range, stats.norm.pdf(range, loc = postmean1, scale= poststd1 ), 
            color='purple', linestyle='-', linewidth=2, label='posterior' )
        P.axvline(self.runpars.data.alpha_l[1], color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        
        ax3 = fig0.add_subplot( 223 )
        ax3.set_xlabel( 'beta parameters' )
        ax3.set_ylabel( 'normal priordensity' )
        ax3.set_title( "" )
        mean2 =  self.runpars.priors.bprior[0]
        std2 =  self.runpars.priors.vprior[0,0]
        P.plot( range, stats.norm.pdf(range, mean2, std2 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
        postmean2 =  np.mean( self.results.b_gibbs[:,0])
        poststd2 =  np.std( self.results.b_gibbs[:,0])
        P.plot( range, stats.norm.pdf(range, loc = postmean2, scale= poststd2 ), 
            color='purple', linestyle='-', linewidth=2, label='posterior' )
        P.axvline(self.runpars.data.beta_lh[0], color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        
        ax4 = fig0.add_subplot( 224 )
        ax4.set_xlabel('beta parameters')
        ax4.set_ylabel('normal prior density')
        ax4.set_title("")
        mean3 = self.runpars.priors.bprior[1]
        std3 = self.runpars.priors.vprior[1,1]
        P.plot( range, stats.norm.pdf(range, mean3, std3 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
        postmean3 =  np.mean( self.results.b_gibbs[:,1])
        poststd3 =  np.std( self.results.b_gibbs[:,1])
        P.plot( range, stats.norm.pdf(range, loc = postmean3, scale= poststd3 ), 
            color='purple', linestyle='-', linewidth=2, label='posterior' )
        P.axvline(self.runpars.data.beta_lh[1], color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        P.tight_layout()
        
        #beta priors for tau and psi 
        fig1 = P.figure(1, figsize = ( 10, 5 ))
        ax1 = fig1.add_subplot( 221 )
        ax1.set_xlabel( 'tau_mh' )
        ax1.set_ylabel( 'beta prior density' )
        ax1.set_title( "" )
        range = np.linspace(-0.2,1.2,1000)
        a0 =  self.runpars.priors.tau_prior[1,0]
        b0 =  self.runpars.priors.tau_prior[1,1]
        P.plot( range, stats.beta.pdf(range, a = a0, b = b0 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
#        P.axvline( np.mean(self.results.tau_gibbs[:,1]) , 
#            color='blue', linestyle='-', linewidth=2, label='posterior' )
        postmean0 =  np.mean( self.results.tau_gibbs[:,1])
        poststd0 =  np.std( self.results.tau_gibbs[:,1])
#        P.plot( range, stats.norm.pdf(range, loc = postmean0, scale= poststd0 ), 
#            color='blue', linestyle='-', linewidth=2, label='posterior' )
        P.hist( self.results.tau_gibbs[:,1], bins = 15, histtype='stepfilled',color= 'blue', normed = False, label='posterior' )
        P.axvline(self.runpars.data.tau_mh, color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        
        ax2 = fig1.add_subplot( 222 )
        ax2.set_xlabel('tah_hl')
        ax2.set_ylabel('beta prior density')
        ax2.set_title("")
        a1 = self.runpars.priors.tau_prior[2,0]
        b1 =  self.runpars.priors.tau_prior[2,1]
        P.plot( range, stats.beta.pdf(range, a1, b1 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
#        P.axvline( np.mean(self.results.tau_gibbs[:,2]) , 
#            color='blue', linestyle='-', linewidth=2, label='posterior' )
        postmean1 =  np.mean( self.results.tau_gibbs[:,2])
        poststd1 =  np.std( self.results.tau_gibbs[:,2])
#        P.plot( range, stats.norm.pdf(range, loc = postmean1, scale= poststd1 ), 
#            color='blue', linestyle='-', linewidth=2, label='posterior' )
        P.hist( self.results.tau_gibbs[:,2], bins = 15, histtype='stepfilled',color= 'blue', normed = False, label='posterior' )
        P.axvline(self.runpars.data.tau_hl, color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()

        ax3 = fig1.add_subplot( 223 )
        ax3.set_xlabel( 'psi_m' )
        ax3.set_ylabel( 'beta prior density' )
        ax3.set_title( "" )
        a2 =  self.runpars.priors.psi_prior[1,0]
        b2 =  self.runpars.priors.psi_prior[1,1]
        P.plot( range, stats.beta.pdf(range, a2, b2 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
#        P.axvline( np.mean(self.results.psi_gibbs[:,1]) , 
#            color='blue', linestyle='-', linewidth=2, label='posterior' )
        postmean2 =  np.mean( self.results.psi_gibbs[:,1])
        poststd2 =  np.std( self.results.psi_gibbs[:,1])
#        P.plot( range, stats.norm.pdf(range, loc = postmean2, scale= poststd2 ), 
#            color='blue', linestyle='-', linewidth=2, label='posterior' )
        P.hist( self.results.psi_gibbs[:,1], bins = 15, histtype='stepfilled',color= 'blue', normed = False, label='posterior' )
        P.axvline(self.runpars.data.psi_m, color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        
        ax4 = fig1.add_subplot( 224 )
        ax4.set_xlabel('psi_h')
        ax4.set_ylabel('beta prior density')
        ax4.set_title("")
        a3 = self.runpars.priors.psi_prior[2,0]
        b3 = self.runpars.priors.psi_prior[2,1]
        P.plot( range, stats.beta.pdf(range, a3, b3 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
#        P.axvline( np.mean( self.results.psi_gibbs[:,2] ) , 
#            color='blue', linestyle='-', linewidth=2, label='posterior' )
        postmean3 =  np.mean( self.results.psi_gibbs[:,2])
        poststd3 =  np.std( self.results.psi_gibbs[:,2])
#        P.plot( range, stats.norm.pdf(range, loc = postmean3, scale= poststd3 ), 
#            color='blue', linestyle='-', linewidth=2, label='posterior' )
        P.hist( self.results.psi_gibbs[:,2], bins = 15, histtype='stepfilled',color= 'blue', normed = False, label='posterior' )
        P.axvline(self.runpars.data.psi_h, color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        P.tight_layout()
        
        #gamma priors for sig of cutoff distributions
        fig3 = P.figure(3, figsize = ( 10, 5 ) )
        ax1 = fig3.add_subplot( 221 )
        ax1.set_xlabel( 'sig^2_lm' )
        ax1.set_ylabel( 'gamma prior density' )
        ax1.set_title( "trapping" )
        range1 = np.linspace(-0.1,0.8,1000)
        range2 = np.linspace(-0.1,0.8,1000)
        shp0 =  self.runpars.priors.gammprior_trap[0,0]
        scl0 =  self.runpars.priors.gammprior_trap[0,1]
        P.plot( range1, stats.gamma.pdf(range1, a = shp0, scale = scl0 ), 
            color='red', linestyle='-', linewidth=1, label='prior' )
        P.axvline( np.mean( self.results.sig_trap_gibbs[0] ),
            color='purple', linestyle='-', linewidth=2, label='posterior' )
        P.axvline(self.runpars.data.sig_trap[0], color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        
        ax2 = fig3.add_subplot( 222 )
        ax2.set_xlabel('sig^2_mh')
        ax2.set_ylabel('gamma prior density')
        ax2.set_title("trapping")
        shp1 =  self.runpars.priors.gammprior_trap[1,0]
        scl1 =  self.runpars.priors.gammprior_trap[1,1]
        P.plot( range2, stats.gamma.pdf(range2, a = shp1, scale = scl1 ), 
            color='red', linestyle='-', linewidth=1, label='prior' )
        P.axvline( np.mean( self.results.sig_trap_gibbs[1] ),
            color='purple', linestyle='-', linewidth=2, label='posterior' )
        P.axvline(self.runpars.data.sig_trap[1], color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()

        ax3 = fig3.add_subplot( 223 )
        ax3.set_xlabel( 'sig^2_lm' )
        ax3.set_ylabel( 'gamma prior density' )
        ax3.set_title( "chewcards" )
        shp2 = self.runpars.priors.gammprior_chew[0,0]
        scl2 =  self.runpars.priors.gammprior_chew[0,1]
        P.plot( range1, stats.gamma.pdf(range1, a = shp2, scale= scl2 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
        P.axvline( np.mean( self.results.sig_chew_gibbs[0] ),
            color='purple', linestyle='-', linewidth=2, label='posterior' )
        P.axvline(self.runpars.data.sig_chew[0], color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()

        ax4 = fig3.add_subplot( 224 )
        ax4.set_xlabel('sig^2_mh')
        ax4.set_ylabel('gamma prior density')
        ax4.set_title("chewcards")
        shp3 = self.runpars.priors.gammprior_chew[1,0]
        scl3 = self.runpars.priors.gammprior_chew[1,1]
        P.plot( range2, stats.gamma.pdf(range2, a = shp3, scale = scl3 ),
            color='red', linestyle='-', linewidth=1, label='prior' )
        P.axvline( np.mean( self.results.sig_chew_gibbs[1] ),
            color='purple', linestyle='-', linewidth=2, label='posterior' )
        P.axvline(self.runpars.data.sig_chew[1], color='k', linestyle='-', linewidth=3, label='true' )
        P.legend()
        P.tight_layout()
        
        P.show()        
        
    def biorship_plots(self):
        """
        comparison of prior, posterior and true relationships
        between rainfall and tau[low-high] and psi[low]
        """
        # relationship between rainfall and psi[low] (from alpha parameters)
        fig0 = P.figure(0, figsize = ( 10, 5 ), facecolor = 'white' )
        ax1 = fig0.add_subplot( 121 )
        ax1.spines['top'].set_visible( False )
        ax1.spines['right'].set_visible( False )
        ax1.yaxis.set_ticks_position( 'left' )
        ax1.xaxis.set_ticks_position('bottom')
        ax1.set_xlabel( 'Rainfall (mL)' )
        ax1.set_ylabel( r'$\Psi^{[0,0]}$', fontsize = 20 )
        ax1.set_title( "" )
        rain = np.linspace( np.min(self.runpars.data.rain), np.max(self.runpars.data.rain), 700 )
        mean_rain = np.mean( rain )
        sd_rain = np.std( rain )
        scl_rain = ( rain - mean_rain ) / ( 2 * sd_rain )                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        x0 = np.ones( rain.shape ) #intercept
        x = np.vstack( [x0, scl_rain] ) #covariate
        xcoefs = np.transpose(x)
#        rain = self.runpars.data.rain
#        xcoefs = self.runpars.data.x
        aprior =  self.runpars.priors.aprior 
        sdprior =  self.runpars.priors.vprior[0,0]
        prior_logitpsi = np.dot(xcoefs, aprior) #logit of med-high transition
        prior_psi = basic.inv_logit(prior_logitpsi) #prob of transitioning from low to high (mouse plague)
        P.plot( rain, prior_psi, linewidth = 1, color='black', 
                linestyle = 'dashed', label = 'prior' )
#        a0 =  np.mean( self.results.a_gibbs[:,0] )
#        a1 =  np.mean( self.results.a_gibbs[:,1] )
#        apost =  np.array( [a0, a1] )
        apost = np.apply_along_axis( np.mean, 0, self.results.a_gibbs )
        post_logitpsi = np.dot(xcoefs, apost) #logit of med-high transition
        post_psi = basic.inv_logit(post_logitpsi) #prob of transitioning from low to high (mouse plague)
        P.plot( rain, post_psi, color='grey', linestyle='-', linewidth=2, label='posterior' )
        areal = self.runpars.data.alpha_l
        real_logitpsi = np.dot(xcoefs, areal )
        real_psi = basic.inv_logit( real_logitpsi )
        P.plot(rain, real_psi, color='k', linestyle='-', linewidth=3, label = 'true' )
#        P.legend()
        P.tight_layout()

        #relationship between rainfall and tau[low-high] (beta parameters)
        ax2 = fig0.add_subplot( 122 )
#        ax2.patch.set_visible( False )
        ax2.spines['top'].set_visible( False )
        ax2.spines['right'].set_visible( False )
        ax2.yaxis.set_ticks_position( 'left' )
        ax2.xaxis.set_ticks_position('bottom')
        ax2.set_xlabel('Rainfall (mL)')
        ax2.set_ylabel( r'$\tau^{[0,2]}$', fontsize = 20)
        ax2.set_title("")
        ax2.set_ylim( [0.0, 1.0] )
        bprior =  self.runpars.priors.bprior 
        prior_logittau = np.dot(xcoefs, bprior) #logit of med-high transition
        prior_tau = basic.inv_logit(prior_logittau) #prob of transitioning from low to high (mouse plague)
        P.plot( rain, prior_tau, linewidth = 1, color ='black', 
                linestyle= 'dashed', label = 'prior' )
#       b0 =  np.mean( self.results.b_gibbs[:,0] )
#       b1 =  np.mean( self.results.b_gibbs[:,1] )
#       bpost =  np.array( [b0, b1] )
        bpost = np.apply_along_axis( np.mean, 0, self.results.b_gibbs )
        post_logittau = np.dot(xcoefs, bpost) #logit of med-high transition
        post_tau = basic.inv_logit(post_logittau) #prob of transitioning from low to high (mouse plague)
        P.plot( rain, post_tau, color='grey', linestyle='-', linewidth=2, label='posterior' )
        breal = self.runpars.data.beta_lh
        real_logittau = np.dot(xcoefs, breal )
        real_tau = basic.inv_logit( real_logittau )
        P.plot(rain, real_tau, color='k', linestyle='-', linewidth=3, label = 'true' )
        P.legend( loc = 'upper left', frameon = False )
#        P.figure( facecolor = 'white' )
        P.tight_layout()
        
        P.show()        

    def abundance_plots( self ):
        """
        sample plots of simulated abundances
        and matching indices
        """
        sites = 100
        T = 16
        K = 5
        sample = np.random.randint( sites, size = 4 )
        print("sample", sample )
        range1 = np.arange(0,T,1)
        nid = self.runpars.nid
        ztrap = self.runpars.data.z_trap
        zchew = self.runpars.data.z_chew
        trap_nid = self.runpars.data.ztrap_nid
        chew_nid = self.runpars.data.zchew_nid
        N_it = self.runpars.data.N_it
#        print( N_it[idx1] )
        kid = np.arange( 0,T*K,K )
        print( 'kid', kid )

        idx1 = np.arange( sample[0], sites*T + sample[0], sites )
#        print( idx1 )
#            print( 'ztrap', ztrap[ np.in1d( trap_nid, idx1 ) ] )
        ztrap_idx1 = ztrap[ np.in1d( trap_nid, idx1 ) ]
        zchew_idx1 = zchew[ np.in1d( chew_nid, idx1 ) ]
#        idx2 = np.arange( sample[1], sites*T + sample[1], sites )
#        idx3 = np.arange( sample[2], sites*T + sample[2], sites )
#        idx4 = np.arange( sample[3], sites*T + sample[3], sites )
#        idx5 = np.arange( sample[4], sites*T + sample[4], sites )
#        P.plot( range, ztrap_idx1[ (kid + 4) ], color='black', 
#                linestyle='-', linewidth=2, label= 'true', 
#                alpha = 0.8 )
        alph = np.linspace( 0.2, 1, K )

        fig0 = P.figure( 0, figsize = ( 10, 5 ), facecolor = 'white' )
        ax1 = fig0.add_subplot( 121 )
        ax1.spines['top'].set_visible( False )
        ax1.spines['right'].set_visible( False )
        ax1.yaxis.set_ticks_position( 'left' )
        ax1.xaxis.set_ticks_position('bottom')
        ax1.set_xlabel( 'T', fontsize = 18 )
        ax1.set_ylabel( 'Captures per 100 trap-nights' )
        ax1.set_title( "" )
#        ax1.set_ylim( [0.0, 25] )

        for k in range(K):        
            print(k)
            P.plot( range1, ztrap_idx1[ (kid + k) ], color='k', 
                    linestyle='-', linewidth=2, label= 'true', 
                    alpha = alph[k]  )

        ax2 = fig0.add_subplot( 122 )
        ax2.spines['top'].set_visible( False )
        ax2.spines['right'].set_visible( False )
        ax2.yaxis.set_ticks_position( 'left' )
        ax2.xaxis.set_ticks_position('bottom')
        ax2.set_xlabel( 'T', fontsize = 18 )
        ax2.set_ylabel( 'Proportion of active chewcards' )
        ax2.set_title( "" )
#        ax2.set_ylim( [0.0, 25] )

        for k in range(K):        
            P.plot( range1, zchew_idx1[ (kid + k) ], color='k', 
                    linestyle='-', linewidth=2, label= 'true', 
                    alpha = alph[k]  )

        P.tight_layout()
        
        P.show()        


    def cutoff_plots(self):
        """
        sample cutoff plots
        """
        #normal priors for trapping cutoff distributions
        fig0 = P.figure( 0, figsize = ( 10, 5 ), facecolor = 'white' )
        ax1 = fig0.add_subplot( 121 )
        ax1.spines['top'].set_visible( False )
        ax1.spines['right'].set_visible( False )
        ax1.yaxis.set_ticks_position( 'left' )
        ax1.xaxis.set_ticks_position('bottom')
        ax1.set_xlabel( r'$\mu^{[0-1]}_{q = 0}$', fontsize = 18 )
        ax1.set_ylabel( '' )
        ax1.set_title( "" )
        ax1.set_ylim( [0.0, 25] )
        range0 = np.linspace(0.0,0.6,1000)
        range1 = np.linspace(0.4,1.0,1000)
#        range0 = np.linspace(-1.5,0.0,1000)
#        range1 = np.linspace(0.0,1.5,1000)
        postmu = np.mean( self.results.mu_trap_gibbs, axis = 0 )
        logitmu = basic.logit( postmu) 
        mugibbs = self.results.mu_trap_gibbs[:-2,:]
        print( 'postmu', postmu, 'logitmu', logitmu )
        print('real mu_trap', self.runpars.data.mu_trap )
        print( 'mean mu_trap0', np.mean( self.results.mu_trap_gibbs, axis = 0) )
#        print( 'mean mu_trap1', np.mean( self.results.mu_trap_gibbs, axis = 1) )
        print('logit post mu_trap', basic.logit( np.mean( self.results.mu_trap_gibbs[:,0]) ) )
        
        mean0 =  self.runpars.priors.logit_muprior_trap[0,0]
        std0 =  self.runpars.priors.logit_muprior_trap[0,1]

        P.hist( mugibbs[:,0], bins = 20, histtype = 'stepfilled',
            alpha = 0.7, color = 'black', label = 'posterior', normed = 1  )
        P.plot( range0, stats.norm.pdf( basic.logit(range0), loc = mean0, 
            scale= std0 ), linewidth = 2, color='grey', 
            linestyle = '-', label = 'prior' )

        realdistb0 = stats.norm.pdf( basic.logit(range0), 
            basic.logit(self.runpars.data.mu_trap[0]), 
            self.runpars.data.sig_trap[0])
         
        P.plot( range0, realdistb0, color='k', linestyle='-', 
            linewidth=2, label= 'true' )
#        postdistb0 = stats.norm.pdf( range0, logitmu[0], 
#                    np.mean(self.results.sig_trap_gibbs[:,0]) )
#        postdistb0 = stats.norm.pdf( range0, basic.logit( np.mean(self.results.mu_trap_gibbs[:,0]) ), 
#                    np.mean(self.results.sig_trap_gibbs[:,0]) )
#        P.plot( range0, postdistb0, color='grey', linestyle='-', linewidth=2, label='posterior' )
#        P.legend()
         
        ax2 = fig0.add_subplot( 122 )
        ax2.spines['top'].set_visible( False )
        ax2.spines['right'].set_visible( False )
        ax2.yaxis.set_ticks_position( 'left' )
        ax2.xaxis.set_ticks_position('bottom')
        ax2.set_xlabel(r'$\mu^{[1-2]}_{q = 0}$', fontsize = 18 )
        ax2.set_ylabel('')
        ax2.set_title("")
        ax2.set_ylim( [0.0, 25] )
        mean1 = self.runpars.priors.logit_muprior_trap[1,0]
        std1 =  self.runpars.priors.logit_muprior_trap[1,1]
        P.hist( mugibbs[:,1], bins = 20, histtype = 'stepfilled',
            alpha = 0.7, color = 'black', label = 'posterior', normed = 1  )
        P.plot( range1, stats.norm.pdf( basic.logit(range1), loc = mean1, 
            scale= std1 ), linewidth = 2, color='grey',
            linestyle = '-', label = 'prior' )
        realdistb1 = stats.norm.pdf( basic.logit(range1), 
            basic.logit(self.runpars.data.mu_trap[1]), 
            self.runpars.data.sig_trap[1])
        P.plot( range1, realdistb1, color='k', linestyle='-', 
            linewidth=2, label = 'true' )
#        postdistb1 = stats.norm.pdf( range1, basic.logit(np.mean(self.results.mu_trap_gibbs[:,1])), 
#                    np.mean(self.results.sig_trap_gibbs[:,1]) )
#        P.plot( range1, postdistb1, color='grey', linestyle='-', linewidth=2, label= 'posterior' )
        P.legend(loc ='best', frameon = False)
        P.tight_layout()

        #normal priors for chewcard cutoff distributions
        fig1 = P.figure(1, figsize = ( 10, 5 ), facecolor="white" )
        ax1 = fig1.add_subplot( 121 )
        ax1.spines['top'].set_visible( False )
        ax1.spines['right'].set_visible( False )
        ax1.yaxis.set_ticks_position( 'left' )
        ax1.xaxis.set_ticks_position('bottom')
        ax1.set_xlabel( r'$\mu^{[0-1]}_{q = 1}$', fontsize = 18 )
        ax1.set_ylabel( '' )
        ax1.set_title( "" )
        ax1.set_ylim( [0.0, 25] )
#        range0 = np.linspace(-1.5,0.0,1000)
#        range1 = np.linspace(0.0,1.5,1000)
        mugibbs = self.results.mu_chew_gibbs[:-2,:]

        mean0 =  self.runpars.priors.logit_muprior_chew[0,0]
        std0 =  self.runpars.priors.logit_muprior_chew[0,1]
        P.hist( mugibbs[:,0], bins = 20, histtype = 'stepfilled',
            alpha = 0.7, color = 'black', label = 'posterior', normed = 1  )
        P.plot( range0, stats.norm.pdf( basic.logit(range0), loc = mean0, 
            scale= std0 ), linewidth = 2, color='grey', 
            linestyle = '-', label = 'prior' )

        realdistb0 = stats.norm.pdf( basic.logit(range0), 
            basic.logit(self.runpars.data.mu_chew[0]), 
            self.runpars.data.sig_chew[0])
         
        P.plot( range0, realdistb0, color='k', linestyle='-', 
            linewidth=2, label= 'true' )
#        P.legend() 

        ax2 = fig1.add_subplot( 122 )
        ax2.spines['top'].set_visible( False )
        ax2.spines['right'].set_visible( False )
        ax2.yaxis.set_ticks_position( 'left' )
        ax2.xaxis.set_ticks_position('bottom')#q=1 chew
        ax2.set_xlabel(r'$\mu^{[1-2]}_{q = 1}$', fontsize = 18)
        ax2.set_ylabel('')
        ax2.set_title("")
        ax2.set_ylim( [0.0, 25] )
        mean1 = self.runpars.priors.logit_muprior_chew[1,0]
        std1 =  self.runpars.priors.logit_muprior_chew[1,1]
        P.hist( mugibbs[:,1], bins = 20, histtype = 'stepfilled',
            alpha = 0.7, color = 'black', label = 'posterior', normed = 1  )
        P.plot( range1, stats.norm.pdf( basic.logit(range1), loc = mean1, 
            scale= std1 ), linewidth = 2, color='grey', 
            linestyle = '-', label = 'prior' )

        realdistb1 = stats.norm.pdf( basic.logit(range1), 
            basic.logit(self.runpars.data.mu_chew[1]), 
            self.runpars.data.sig_chew[1])
         
        P.plot( range1, realdistb1, color='k', linestyle='-', 
            linewidth=2, label= 'true' )
        P.legend( loc ='best',frameon = False )
        P.tight_layout()
        
        P.show()        



    def plotFX(self):
        """
        make diagnostic trace plots
        """
        #process parameters
        realpsi = np.array( [self.runpars.data.psi_m, self.runpars.data.psi_h] )
        realtau = np.array( [self.runpars.data.tau_mh, self.runpars.data.tau_hl] )
        xrange = [0,300]
        P.figure(0)
        for i in range(2):
            P.subplot(2, 2, i+1)
            P.plot(self.results.b_gibbs[:-1, i], label= 'posterior' )#ax = axes[i] )
#            P.ylim( [-6.0, 6.0] ) 
            P.plot(xrange, [self.runpars.data.beta_lh[i], self.runpars.data.beta_lh[i]], 
                label = 'true beta', color = 'k', linestyle = '-', linewidth = 3 )
            P.legend()

            P.subplot(2, 2, i+3)
            P.plot(self.results.a_gibbs[:-1, i], label = 'posterior' )
#            P.ylim( [-8.0, 6.0] ) 
            P.plot(xrange, [self.runpars.data.alpha_l[i], self.runpars.data.alpha_l[i]], 
                label = 'true alpha', color = 'k', linestyle = '-', linewidth = 3 )
            P.legend()
            
        P.figure(1)
        for i in range(2):
            P.subplot(2,2,i+1 )
            P.plot( self.results.psi_gibbs[:-1,1+i], label ='posterior' )
            P.plot(xrange, [realpsi[i], realpsi[i]], 
                label = 'true psi', color = 'k', linestyle = '-', linewidth = 3 )
            P.legend()
            
            P.subplot(2,2,i+3)
            P.plot( self.results.tau_gibbs[:-1,1+i], label = 'posterior' )
            P.plot(xrange, [realtau[i], realtau[i]], 
                label = 'true tau',  color = 'k', linestyle = '-', linewidth = 3 )
            P.legend()
        #observation parameters
        P.figure(2)
        for i in range(2):
            P.subplot(2, 2, i+1)
            P.plot(self.results.mu_trap_gibbs[:-1, i], 
                label = 'posterior' )#, ax = axes[i] )
#            axes[0].set_ylabel('mu l-m')
#            axes[1].set_ylabel('mu m-h')
#            P.ylim( [0.1, 0.5] ) 
            P.plot(xrange, [self.runpars.data.mu_trap[i], self.runpars.data.mu_trap[i]], 
                label = 'true trapping mu', color = 'k', linestyle = '-', linewidth = 3 )
            P.legend()
            P.subplot(2, 2, i+3)
            P.plot(self.results.sig_trap_gibbs[:-1, i], 
                label = 'posterior', linewidth = 3 )
#            P.ylim( [0.0, 0.5] ) 
            P.plot(xrange, [self.runpars.data.sig_trap[i], self.runpars.data.sig_trap[i]], 
                label = 'true trapping sigma', color = 'k', linestyle = '-', linewidth = 3 )
            P.legend()
        P.figure(3)
        for i in range(2):
            P.subplot(2,2,i+1 )
            P.plot( self.results.mu_chew_gibbs[:-1,i], label = 'posterior' )
#            P.ylim( [0.02, 0.5] ) 
            P.plot(xrange, [self.runpars.data.mu_chew[i], self.runpars.data.mu_chew[i]], 
                label = 'true chewcard mu', color = 'k', linestyle = '-', linewidth = 3 )
            P.legend()
            P.subplot(2,2,i+3)
            P.plot( self.results.sig_chew_gibbs[:-1,i], 
                label = 'posterior', linewidth = 3 )
#            P.ylim( [0.0, 0.6] ) 
            P.plot(xrange, [self.runpars.data.sig_chew[i], self.runpars.data.sig_chew[i]], 
                label = 'true chewcard sigma', color = 'k', linestyle = '-', linewidth = 3 )
            P.legend()
        P.show()
#        P.scatter(self.params.y, Ypredicted)
                                                                                                             
                                                                                                             

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--resultsfile", dest="resultsfile", help="Input pickled results file")
        p.add_option("--runparsfile", dest="runparsfile", help="Input pickled runpars file")
#        p.add_option("--tablefile", dest="tablefile", help="output tablefile")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)
                                    



########            Main function
#######
def main( ):
    cmdargs = CmdArgs()
    print(cmdargs.resultsfile)
    
#    trialresults = PerfResults( pklresults = cmdargs.resultsfile, pklrunpars = cmdargs.runparsfile )
    post = Postprocess(pklresults = cmdargs.resultsfile, pklrunpars = cmdargs.runparsfile )
#    post.summTable()
#    post.SummStats()
#    post.Ncomps()
#   post.SummZdata()
#    post.plotFX()
#    post.cutoff_plots()
#    post.param_distb_plots()
    post.biorship_plots()
#    post.abundance_plots()

if __name__ == '__main__':
        main()
