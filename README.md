# README #

## **Code from Cruz et al. (in review) 'Extracting more out of indices of abundance using multi-state occupancy models with boundary distributions'. Global Change Ecology and Conservation.** 

### What is this repository for? ###
In the manuscript we propose a new Bayesian (multi-state, multi-season) occupancy model that estimates the probabilities of abundance shifting among alternative states (i.e. low, medium and high) by using continuous index data collected under a robust approach. The multi-state occupancy model is composed of two parts: 
(1) an observation sub-model that uses prior knowledge and boundary distributions to partition data into abundance states, and 
(2) a biological sub-model that relates the probabilities of transitioning among abundance states to environmental predictors. 

The repository contains scripts to: 

1) generate abundance state data from a simulated mouse dynamics process and, with a virtual ecologist approach, sample these simulated data using two techniques: trapping (expressed as captures per 100 traps) and proportion of active chew cards (with sign of mouse activity)
2) use the simulated data to estimate model parameters using a purposely-written MCMC algorithm that uses a combination of Gibbs and Metropolis-Hastings
3) assess model performance under the 12 scenarios outlined in the manuscript by estimating coverage and relative bias
4) summarise detailed model output for a single chain run for one scenario including plotting posterior distributions

### How do I get set up? ###

Script details: 

- basic.py contains basic functions used by the other scripts
- simdata.py simulates mouse data
- params.py generates starting values and priors for the mcmc
- mcmc.py contains the algorithm that updates model parameters and a function to save results and to calculate performance statistics
- process_perresults.py _summarises performance results into output tables 
- postprocess.py summarises results for a single chain run for one scenario
- send.py calls appropriate scripts to estimate model parameters for a single scenario and save results as pickle files
- process.py calls appropriate scripts to run performance testing for multiple scenarios
- submit.py contains parameters for alternative scenarios used in performance testing

Deployment instructions: will be coming soon 

### Who do I talk to? ###

Email cruzbernalj@landcareresearch.co.nz for any questions.