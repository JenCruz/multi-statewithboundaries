#!/usr/bin/env python
#
'''

Script written by Dr Jennyffer Cruz 
Updates the mcmc for a multi-state multi-season occupancy model 
The script uses simulated mouse data that assumes some transition probabilities
are dependent on rainfall and that all sites are always occupied at either:
low, med or high abundance.
Observation data are continuous indices of trap success and chew card records

'''

#importing required modules and python scipts
import os
import basic #this imports basic.py with all our basic functions
import simdata #simulated data
import params #params script with priors, params, MCMCparams
import numpy as np
import random
from scipy import stats
import pickle


class RunPars(object):
    '''
    Stores updated parameters needed to re-start an MCMC 
    using initial parameter estimates from params.py
    attributes:
    pars = initial parameter estimates from Pars()
    data = from SimData() 
    '''
    def __init__( self, pars = None, priors = None, mcmcpars = None ): 
        if pars is None:
            pars = params.Pars() 
        else:
            pars = pars    
        self.data = pars.data
        if priors is None:
            self.priors = params.Priors()
        else: 
            self.priors = priors
        if mcmcpars is None:    
            self.mcmcpars = params.MCMCPars()
        else:
            self.mcmcpars = mcmcpars
        ####### #process submodel parameters:  #############
        self.b = pars.b.copy() #function pars for transition from low to med
        self.a = pars.a.copy() #function pars for prob of remaining in low
        self.Tau = pars.Tau.copy() #m-h and h-l transition probs
        self.Tau_lh = pars.Tau_lh.copy() #transition prob low-high (plague)
        self.Psi = pars.Psi.copy() #psi_m & psi_h are the only with real parameters
        self.Psi_l = pars.Psi_l.copy() #prob of remaining in low

        self.Phi = pars.Phi.copy() #initial phi estimate

        self.cmat = pars.cmat #jump distribution for metropolis
        self.Tjump = pars.Tjump #jump distribution for metropolis-hastings of Tau_hl
        self.Pjump = pars.Pjump #jump distribution for Psi 

        ########## #observation submodel parameters: ###########
        self.mujump = pars.mujump #jump distribution for metropolis-hastings of Tau_hl
        self.sigjump = pars.sigjump #jump distribution for Psi 

        self.mu_trap = pars.mu_trap.copy() #mean of cut-off distributions for trapping
        self.sig_trap = pars.sig_trap.copy() #sd of errors for trapping
        
        self.mu_chew = pars.mu_chew.copy() #mean of cut-off distributions for trapping
        self.sig_chew = pars.sig_chew.copy() #sd of errors for trapping

        self.prZ_trap = pars.prZ_trap.copy() #prob trapping data is in each given abund states
        self.prZ_chew = pars.prZ_chew.copy()        
        
        self.N0 = pars.N0.copy() #1d array of initial abundance state (Nt)
        self.N1 = pars.N1.copy() #1d array of final abundance state (Nt+1)
        self.Nit = pars.Nit.copy() #id N array for all sites*T

        self.ztrapid = self.data.ztrapid #id for each survey (for all sites & T)
        self.zchewid =  self.data.zchewid
        self.ztrap_nid = self.data.ztrap_nid #id for each site & T in z arrays (equal for multiple surveys of the same site &T)
        self.zchew_nid = self.data.zchew_nid
        self.nid = self.data.nid #id for each site & T in Nit array
        self.ind = np.arange( self.Phi.shape[0] )#id for each site & T in Phi array                        
        self.NAtrap = self.data.NAtrap
        self.NAchew = self.data.NAchew
                
#defining MCMC sampler
class MCMC(object):
    '''
    Defines MCMC updating functions and arrays for storing predicted coefficients
    attributes: 
    data = using simulated data from SimData(), 
    priors = using preset priors from Priors()
    runpars =  values that are updated during an MCMC run and either come from 
    RunPars() if initial run or from an updated pickle file
    mcmcpars = MCMC parameters from MCMCPars
    these values can be altered by MCMC( data = Data(n = 200)) 
    '''
    def __init__(self, runpars ): 
        self.runpars = runpars    
        self.data = self.runpars.data
        self.priors = self.runpars.priors
        self.mcmcpars = self.runpars.mcmcpars
                
        #process submodel parameters to keep:
        self.b_gibbs = np.empty( [ len(self.mcmcpars.keep), 2 ] ) #transition prob model coeffs
        self.a_gibbs = np.empty( [ len(self.mcmcpars.keep), 2 ] )#coef for prob of staying in low
        self.tau_gibbs = np.empty( [ len(self.mcmcpars.keep), 3 ] ) #m-h & h-l transition par
        self.psi_gibbs = np.empty( [ len(self.mcmcpars.keep), 3 ] ) #prob of remaining in m or h
        self.psi_l_gibbs = np.empty( [ len(self.mcmcpars.keep), ( self.data.sites * (self.data.T-1) ) ] )    
        self.tau_lh_gibbs = np.empty( [ len(self.mcmcpars.keep), ( self.data.sites * (self.data.T-1) ) ] )    
        
        #observation submodel parameters to keep:
        self.mu_trap_gibbs = np.empty( [ len(self.mcmcpars.keep), 2 ] ) 
        self.sig_trap_gibbs = np.empty( [ len(self.mcmcpars.keep), 2 ] )
        self.mu_chew_gibbs = np.empty( [ len(self.mcmcpars.keep), 2 ] ) 
        self.sig_chew_gibbs = np.empty( [ len(self.mcmcpars.keep), 2 ] )

        self.Nit_gibbs =  np.empty( [ len(self.mcmcpars.keep), ( 
            self.data.sites * self.data.T ) ] ) #latent abundance states
        self.gibbs()


    def cutoff_mu_update( self, q, distb ):
        '''
        Updates the latent abundance states
        based on data and boundary distributions
        attributes:
        q = monitoring method: 0 = trapping, 1 = chewcards
        distb = boundary distribution: 0 = low-med, 1 = med-high
        '''
        if q  == 0:
            z = self.data.z_trap.copy() #data
            mu = self.runpars.mu_trap.copy() #means of boundary distributions
            sig = self.runpars.sig_trap.copy() #SD boundary distributions
            logit_muprior = self.priors.logit_muprior_trap.copy() #normal prior for mean of boundary distribution on logit scale
            prZ = self.runpars.prZ_trap.copy() #prob of data given N = 0,1,2
            zid = self.runpars.ztrapid.copy() #survey id for all sites*T (for z array)
            z_nid = self.runpars.ztrap_nid.copy() #site*T ID for z array (repeat ID for multiple surveys within same site*T)
            
        else:
            z = self.data.z_chew.copy()
            mu = self.runpars.mu_chew.copy()
            sig = self.runpars.sig_chew.copy()
            logit_muprior = self.priors.logit_muprior_chew.copy()
            prZ = self.runpars.prZ_chew.copy()
            zid = self.runpars.zchewid.copy()
            z_nid = self.runpars.zchew_nid.copy()

        z_Nit = self.runpars.Nit[ z_nid ] #expand abundance to length of z array
        z_obvNit = np.zeros( (zid.shape[0], 3), ) #extend to 2D array
        z_obvNit[ zid, z_Nit ] = 1 #assign 1 to current N
            
        #current parameters on logit scale:
        logit_mu = basic.logit( mu )
        #new proposed parameters
        logitmu_s = np.random.normal( logit_mu, self.runpars.mujump )
        
        logitmu_comb = logit_mu.copy() #since we are only updating one distribution at a time we need to combine proposed and current parameters
        logitmu_comb[ distb ] = logitmu_s[ distb ]
        
        def get_prZs( z, logitmu, sig ):
            '''
            This function updates probability of data given N = 0,1,2 
            using two boundary distributions
            attributes:
            z = monitoring data 
            logitmu = mean of boundary distribution on logit scale
            sig = current error of boundary distrib on logit scale
            '''
            logitz = basic.logit( z )
            cdf01 = stats.norm.cdf( logitz , logitmu[0], sig[0] )
            cdf12 = stats.norm.cdf( logitz , logitmu[1], sig[1] ) 
            prZN0 = ( 1 - cdf01 ) * ( 1 - cdf12 ) #prob of data given N = 0
            prZN1 = cdf01 * ( 1 - cdf12 ) #prob of data given N = 1
            prZN2 = cdf12 * cdf01 #prob of data given N = 2
            pvals = np.vstack( [prZN0, prZN1, prZN2] )
            pvals = np.transpose( pvals ) #2d array of prob of data given N = 0, 1, 2. Size= [500,3] 
            pvals[pvals < 1.0e-10 ] = 1.0e-10
            return pvals
            
                        
        prZ_s = get_prZs( z, logitmu = logitmu_comb, sig = sig )
        
        
        def likelihoods():
            '''
            Defines proposed and current likelihoods (log probabilities)
            '''    
            #proposed:
            prZ_s_2d = np.power( prZ_s, z_obvNit )
            prZ_s_1d = np.prod( prZ_s_2d, axis = 1 )

            logprZ_s = np.log( prZ_s_1d )
            lnorm_s = stats.norm.logpdf( x = logitmu_s[distb], loc = 
                logit_muprior[distb,0], scale = logit_muprior[distb,1] ) 
            pnew = np.sum(logprZ_s) + lnorm_s  
            #current:
            prZ_2d = np.power(prZ, z_obvNit )
            prZ_1d = np.prod( prZ_2d, axis = 1 )

            logprZ = np.log( prZ_1d )
            lnorm = stats.norm.logpdf( x = logit_mu[distb], loc = 
                logit_muprior[ distb, 0 ], scale = logit_muprior[ distb, 1 ] ) 
            pnow = np.sum(logprZ) + lnorm
            
            def accept():
                '''
                Updates the parameter values if proposed likelihood is 
                better than current likelihood
                '''
                r = np.exp( pnew -  pnow ) #acceptance criterion
                z = np.random.uniform( 1, 0, size = 1 )
        
                if z < r: #if accept with probability r update b & theta
                    if q == 0:
                        self.runpars.prZ_trap = prZ_s
                        self.runpars.mu_trap = basic.inv_logit( logitmu_comb )
                    else:
                        self.runpars.prZ_chew = prZ_s
                        self.runpars.mu_chew = basic.inv_logit( logitmu_comb )
                    
            accept()
        likelihoods()

    def cutoff_sig_update( self, q, distb ):
        '''
        Updates the latent abundance states
        based on data and cutoff distributions
        attributes:
        q = monitoring method: 0 = trapping, 1 = chewcards
        distb = cutoff distribution: 0 = low-med, 1 = med-high
        '''
        if q  == 0:
            z = self.data.z_trap #data
            mu = self.runpars.mu_trap #means of boundaries
            sig = self.runpars.sig_trap #errors of boundaries
            gammprior = self.priors.gammprior_trap #prior for error of boundaries in logit scale
            prZ = self.runpars.prZ_trap #prob of data given N = 0,1,2 
            zid = self.runpars.ztrapid
            z_nid = self.runpars.ztrap_nid

        else:
            z = self.data.z_chew
            mu = self.runpars.mu_chew
            sig = self.runpars.sig_chew
            gammprior = self.priors.gammprior_chew
            prZ = self.runpars.prZ_chew
            zid = self.runpars.zchewid
            z_nid = self.runpars.zchew_nid
            

        z_Nit = self.runpars.Nit[ z_nid ] #expand abundance to length of z array
        z_obvNit = np.zeros( (zid.shape[0], 3) ) #extend to 2D array
        z_obvNit[ zid, z_Nit ] = 1 #assign 1 to current N

        #current parameters on logit scale:
        logit_mu = basic.logit( mu )

        #new proposed parameters
        sig_s = np.exp( np.random.normal( np.log(sig), self.runpars.sigjump ) )
        
        sig_comb = sig.copy() #since we are only updating one distribution at a time we need to combine proposed and current parameters
        sig_comb[distb] = sig_s[distb]

        def get_prZs( z, logitmu, sig ):
            '''
            This function updates probability of data given N = 0,1,2 
            using two boundary distributions
            attributes:
            z = monitoring data 
            logitmu = mean of boundary distribution on logit scale
            sig = current error of boundary distrib on logit scale
            '''
            logitz = basic.logit( z )
            cdf01 = stats.norm.cdf( logitz , logitmu[0], sig[0] )
            cdf12 = stats.norm.cdf( logitz , logitmu[1], sig[1] ) 
            prZN0 = ( 1 - cdf01 ) * ( 1 - cdf12 ) #prob of data given N = 0
            prZN1 = cdf01 * ( 1 - cdf12 ) #prob of data given N = 1
            prZN2 = cdf12 * cdf01 #prob of data given N = 2
            pvals = np.vstack( [prZN0, prZN1, prZN2] )
            pvals = np.transpose( pvals ) #2d array of prob of data given N = 0, 1, 2
            pvals[pvals < 1.0e-10 ] = 1.0e-10
            return pvals

        #calculate proposed prob that data falls in a given abundance state, 
        #given cut off distributions:    
        prZ_s = get_prZs( z, logitmu = logit_mu, sig = sig_comb )

        
        def likelihoods():
            '''
            Defines proposed and current likelihoods (log probabilities)
            '''    
            #proposed:
            prZ_s_2d = np.power( prZ_s, z_obvNit )
            prZ_s_1d = np.prod( prZ_s_2d, axis = 1 )

            logprZ_s = np.log( prZ_s_1d )
            lgamm_s = stats.gamma.logpdf( x = sig_s[distb], a = 
                gammprior[ distb, 0 ], scale = gammprior[ distb, 1 ] )
 
            pnew = np.sum(logprZ_s) + lgamm_s  
 
            #current:
            prZ_2d = np.power(prZ, z_obvNit )
            prZ_1d = np.prod( prZ_2d, axis = 1 )

            logprZ = np.log( prZ_1d )
            lgamm = stats.gamma.logpdf( x = sig[distb], a = 
                gammprior[distb, 0], scale =  gammprior[distb,1] )
 
            pnow = np.sum( logprZ ) + lgamm
            
            def accept():
                '''
                Updates the parameter values if proposed likelihood is 
                better than current likelihood
                '''
                r = np.exp( pnew -  pnow ) #acceptance criterion
                z = np.random.uniform( 1, 0, size = 1 )
        
                if z < r: #if accept with probability r update parameters
                    if q == 0:
                        self.runpars.prZ_trap = prZ_s
                        self.runpars.sig_trap = sig_comb
                    else:
                        self.runpars.prZ_chew = prZ_s
                        self.runpars.sig_chew = sig_comb
                    
            accept()
        likelihoods()

    def N_update( self ):
        '''
        Updates latent abundance states using observation and 
        process submodels
        '''
        
        for i in self.runpars.nid:
            N_s = self.runpars.Nit.copy()
            N0_s = N_s[ :(-self.data.sites) ] #1D array of Nt        
            N1_s = N_s[ self.data.sites: ] #1D array of Nt+1

#           generate new latent abundance states:
            if N_s[i] == 0:
                N_s[i] = np.random.randint( 1, 3 )
            elif N_s[i] == 1:
                N_s[i] =  random.randrange(0,3,2)
            else:
                N_s[i] = np.random.randint( 0, 2 )

            N0_s = N_s[ :(-self.data.sites) ] #1D array of Nt        
            N1_s = N_s[ self.data.sites: ] #1D array of Nt+1
            
            obvNit = np.zeros( (1,3) )
            obvNit[ :, self.runpars.Nit[i] ] = 1
            obvN_s = np.zeros( (1,3) )
            obvN_s[ :, N_s[i] ] = 1 #2D array of all Ns
            
            if self.runpars.NAtrap[i] == 1:
                prZ0_trap = np.ones( (1,3) )
            else:
                prZ0_trap = self.runpars.prZ_trap[ self.runpars.ztrap_nid == i, : ]
                
                prZ0_trap = np.prod( prZ0_trap, axis = 0 )    
                
            if self.runpars.NAchew[i] == 1:
                prZ0_chew = np.ones( (1,3) )
            else:
                prZ0_chew = self.runpars.prZ_chew[ self.runpars.zchew_nid == i, : ]
                
                prZ0_chew = np.prod( prZ0_chew, axis = 0 )

            prZ = prZ0_trap * prZ0_chew
            
            #converting current prZ to 1D array depending on N
            prZ_2d = np.power( prZ, obvNit )
            prZ_1d = np.prod( prZ_2d )

            if prZ_1d < 1.0e-10:
                logprZ = np.log( 1.0e-10 )
            else:
                logprZ = np.log( prZ_1d )
            
            #proposed prZs
            prZs_2d = np.power( prZ, obvN_s )
            prZs_1d = np.prod( prZs_2d )

            if prZs_1d < 1.0e-10:
                lprZ_s = np.log( 1.0e-10 )
            else:
                lprZ_s = np.log( prZs_1d )
                            

            if self.runpars.NAtrap[i] == 1 and self.runpars.NAchew[i] == 1:
                logprZ = 0
                lprZ_s = 0
            
            def likelihoods():
                '''
                Defines proposed and current likelihoods (log probabilities)
                '''    
                if i < self.data.sites:
                    phi_t1 = self.runpars.Phi[ self.runpars.ind[i], 
                        self.runpars.N0[i], self.runpars.N1[i] ] #phi(t+1)
                    logphi1 = np.log( phi_t1 )
                    logphi0 = 0
                    phi1_s = self.runpars.Phi[ self.runpars.ind[i], 
                        N0_s[i], N1_s[i] ] 
                    lphi1_s = np.log( phi1_s )
                    lphi0_s = 0

                elif i > ( self.data.sites - 1 ) and i < (self.data.sites * (self.data.T-1) ):
                    phi_t0 = self.runpars.Phi[ self.runpars.ind[(i-self.data.sites)], 
                            self.runpars.N0[(i - self.data.sites)], self.runpars.N1[(i - self.data.sites)] ] 
                    logphi0 = np.log( phi_t0 )
                    phi_t1 = self.runpars.Phi[ self.runpars.ind[i], 
                            self.runpars.N0[i], self.runpars.N1[i] ] 
                    logphi1 = np.log( phi_t1 )

                    phi0_s = self.runpars.Phi[ self.runpars.ind[ (i - self.data.sites) ], 
                        N0_s[ (i - self.data.sites) ], N1_s[ (i - self.data.sites) ] ]
                    phi1_s = self.runpars.Phi[ self.runpars.ind[i], N0_s[i], N1_s[i] ]
                    lphi0_s = np.log( phi0_s )
                    lphi1_s = np.log( phi1_s )

                else:    
                    phi_t0 = self.runpars.Phi[ self.runpars.ind[ (i - self.data.sites ) ], 
                        self.runpars.N0[ (i - self.data.sites) ], self.runpars.N1[ (i - self.data.sites) ] ] #phi(t)
                    logphi0 = np.log( phi_t0 )
                    logphi1 = 0
                    phi0_s = self.runpars.Phi[ self.runpars.ind[(i - self.data.sites) ], 
                        N0_s[(i - self.data.sites) ], N1_s[ (i - self.data.sites) ] ] 
                    lphi0_s = np.log( phi0_s )
                    lphi1_s = 0

                pnew =  lprZ_s + lphi0_s +  lphi1_s 
                pnow = logprZ +  logphi0  +  logphi1 
            
                def accept():
                    '''
                    Updates the parameter values if proposed likelihood is 
                    better than current likelihood
                    '''
                    r = np.exp( pnew -  pnow ) #acceptance criterion
                    z = np.random.uniform( 1, 0, size = 1 )
            
                    if z < r: #if accept with probability r update parameters
                        self.runpars.N0 = N0_s
                        self.runpars.N1 = N1_s
                        self.runpars.Nit = N_s
                    
                accept()
            likelihoods()

        
    #generic updater for beta coefficients in transition models:    
    def ab_update(self, coefs, betas ):
        '''
        Updating the betas or alphas in linear functions attached to psi and  
        tau when initial abundance state is low = 0
        attributes:
        n = initial abundance state( Phis set for low = 0 )
        coefs = either self.runpars.b or self.runpars.a depending on 
        whether tau or psi are being updated respectively
        betas = 1 if betas updated or 0 if alphas updated
        '''
        #propose new values by updating betas:
        prop = np.random.multivariate_normal( mean = coefs, 
            cov = self.runpars.cmat )
        logit_prop_s = np.dot( self.data.x, prop )
        Prop_s = basic.inv_logit( logit_prop_s )
        
        if betas == 1:
            tau = Prop_s
            psi = self.runpars.Psi_l
            prior = self.priors.bprior

        else:
            tau = self.runpars.Tau_lh
            psi = Prop_s
            prior = self.priors.aprior


        def update_Phi_s( psi, tau ):
            '''
            Updates phi_s for N_t0 = 0 based on new proposed a_s or b_s
            '''
            Phi_s = self.runpars.Phi.copy()
            Phi_s[:,0,1] = ( 1 - psi ) * (1 - tau )
            Phi_s[:,0,2] = ( 1 - psi ) * tau
            if betas == 0:
                Phi_s[:,0,0] = psi
                            
            return Phi_s

        Phi_s = update_Phi_s( psi = psi, tau = tau )
                
        def likelihoods( prior = prior ):
            '''
            Defines proposed and current likelihoods (log probabilities)
            '''    
            #proposed:
            logphi_s = np.log( Phi_s[ self.runpars.ind, self.runpars.N0, self.runpars.N1 ] )
            lnorm_s = stats.multivariate_normal.logpdf( x = prop, mean = 
                prior, cov = self.priors.vprior )
            #current:
            logphi = np.log( self.runpars.Phi[ self.runpars.ind, self.runpars.N0, self.runpars.N1 ] )
            lnorm = stats.multivariate_normal.logpdf( x = coefs ,
                    mean = prior, cov = self.priors.vprior )
                
            if betas == 1:     
                pnew = np.sum(logphi_s[ (self.runpars.N0 == 0) & (
                    self.runpars.N1 != 0) ] ) + lnorm_s 
                pnow = np.sum(logphi[  (self.runpars.N0 == 0 ) & (
                    self.runpars.N1 != 0) ] ) + lnorm  
            else:
                pnew = np.sum(logphi_s[ self.runpars.N0 == 0 ] ) + lnorm_s 
                pnow = np.sum(logphi[ self.runpars.N0 == 0 ] ) + lnorm  

            def accept():
                '''
                Updates the parameter values if proposed likelihood is 
                better than current likelihood
                '''
                r = np.exp( pnew -  pnow ) #acceptance criterion
                z = np.random.uniform( 1, 0, size = 1 )
        
                if z < r: #if accept with probability r update b & theta
                    self.runpars.Phi = Phi_s

                    if betas == 1:
                        self.runpars.b = prop
                        self.runpars.Tau_lh[ (self.runpars.N0 == 0) & 
                            (self.runpars.N1 != 0) ] = Prop_s[ (
                            self.runpars.N0 == 0) & (self.runpars.N1 != 0) ]
                    else:
                        self.runpars.a = prop
                        self.runpars.Psi_l[ self.runpars.N0 == 0 ] = Prop_s[ (
                            self.runpars.N0 == 0) ]
                
            accept()
        likelihoods()
        #                
                
    def tau_update( self, n ):
        '''
        Updates Tau when no logit model is attached to it using a Metropolis 
        Hastings step as Tau has a beta prior 
        (so jumps cannot be simmetrical)
        Attributes:
        n = initial abundance state determines which row of 
        transition probability matrix is updated
        '''
        a_j = basic.beta_a( mean = self.runpars.Tau[n], var = self.runpars.Tjump**2 )
        b_j = basic.beta_b( mean = self.runpars.Tau[n], var = self.runpars.Tjump**2 )
        Tau_s = np.random.beta( a = a_j , b = b_j )
        a_s = basic.beta_a( Tau_s, self.runpars.Tjump**2 )
        b_s = basic.beta_b( Tau_s, self.runpars.Tjump**2 )
        
        Phi_s = self.runpars.Phi.copy()

        def update_Phi_s ( psi, tau ):
            '''
            Updates phi_s based on new proposed tau_s
            '''
            if n == 0:
                Phi_s[:,0,1] = ( 1 - psi ) * ( 1 - tau )
                Phi_s[:,0,2 ] = ( 1 - psi ) * tau 
            elif n == 1:
                Phi_s[:,1,2] = ( 1 - psi ) * tau
                Phi_s[:,1,0] = ( 1 - psi ) * ( 1 - tau )

            else:
                Phi_s[:,2,0] = ( 1 - psi ) * tau
                Phi_s[:,2,1] = ( 1 - psi ) * ( 1 - tau ) 

            return Phi_s
            
        update_Phi_s( psi = self.runpars.Psi[n], tau = Tau_s )

        def likelihoods():
            '''
            Defines proposed and current likelihoods (log probabilities)
            '''    
            #proposed:
            logphi_s = np.log( Phi_s[ self.runpars.ind, self.runpars.N0, self.runpars.N1 ] )
            ljump_s = stats.beta.logpdf( x = self.runpars.Tau[n], a = a_s , b = b_s )
            lbeta_s = stats.beta.logpdf( x = Tau_s,  a = self.priors.tau_prior[n,0], 
                        b = self.priors.tau_prior[n,1] )
            sum_logphi_s = np.sum( logphi_s [ ( self.runpars.N0==n ) & (self.runpars.N1!=n ) ] ) 
            pnew = sum_logphi_s + lbeta_s + ljump_s 
            #current:
            logphi = np.log( self.runpars.Phi[ self.runpars.ind, self.runpars.N0, self.runpars.N1 ] )
            ljump = stats.beta.logpdf( x = Tau_s, a = a_j, b = b_j )
            lbeta = stats.beta.logpdf( x = self.runpars.Tau[n],
                    a = self.priors.tau_prior[n,0], b = self.priors.tau_prior[n,1] )
            sum_logphi = np.sum( logphi[(self.runpars.N0==n) & (self.runpars.N1!=n) ] )
            pnow = sum_logphi + lbeta + ljump  

            def accept():
                '''
                Updates the parameter values if proposed likelihood is 
                better than current likelihood
                '''
                r = np.exp( pnew -  pnow ) #acceptance criterion
                z = np.random.uniform( 1, 0, size = 1 )
        
                if z < r: #if accept with probability r update b & theta
                    self.runpars.Tau[n] = Tau_s
                    self.runpars.Phi = Phi_s
                    
            accept()
        likelihoods()
        
    def psi_update( self, n ):
        '''
        Updates phi parameters using a Metropolis Hasting step and 
        conjugate Beta priors
        Attributes:
        n = abundance states: either low (0), med (1) or high (2)
        '''
        #propose new psi values
        a_j = basic.beta_a( mean = self.runpars.Psi[n], 
                var = self.runpars.Pjump**2 )
        b_j = basic.beta_b( mean = self.runpars.Psi[n], 
                var = self.runpars.Pjump**2 )
        Psi_s = np.random.beta( a = a_j , b = b_j )
        a_s = basic.beta_a( Psi_s, self.runpars.Pjump**2 )
        b_s = basic.beta_b( Psi_s, self.runpars.Pjump**2 )

        Phi_s = self.runpars.Phi.copy()

        def update_Phi_s ( psi, tau ):
            '''
            Updates phi_s based on new proposed psi_s
            '''
            if n == 0:
                Phi_s[:,0,1] = ( 1 - psi ) * ( 1 - tau )
                Phi_s[:,n,n] = psi
                Phi_s[:,0,2 ] = ( 1 - psi ) * tau 

            elif n == 1:
                Phi_s[:,1,0] = ( 1 - psi ) * ( 1 - tau )
                Phi_s[:,n,n] = psi
                Phi_s[:,1,2] = ( 1 - psi ) * tau

            else:
                Phi_s[:,2,0] = ( 1 - psi ) * tau
                Phi_s[:,2,1] = ( 1 - psi ) * ( 1 - tau ) 
                Phi_s[:,n,n] = psi
                
            return Phi_s

        update_Phi_s( psi = Psi_s, tau = self.runpars.Tau[n] )
        
        def likelihoods():
            '''
            Defines proposed and current likelihoods (log probabilities)
            '''    
            #proposed:
            logphi_s = np.log( Phi_s[ self.runpars.ind, self.runpars.N0, self.runpars.N1 ]  )
            ljump_s = stats.beta.logpdf( x = self.runpars.Psi[n], a = a_s,  b = b_s )
            lbeta_s = stats.beta.logpdf( x = Psi_s, 
                a = self.priors.psi_prior[n,0], b = self.priors.psi_prior[n,1] )
            sum_logphi_s = np.sum( logphi_s[(self.runpars.N0 == n)] ) 
            pnew = sum_logphi_s + lbeta_s + ljump_s 

            #current:
            #log prob of phi given tau and psi:
            logphi = np.log( self.runpars.Phi[ self.runpars.ind, self.runpars.N0, self.runpars.N1 ]  )
            #log jump probability:
            ljump = stats.beta.logpdf( x = Psi_s, a = a_j , b = b_j )
            #log prior:
            lbeta = stats.beta.logpdf( x = self.runpars.Psi[n],
                a = self.priors.psi_prior[n,0], b = self.priors.psi_prior[n,1] )
            #log prob of parameters given data:
            sum_logphi = np.sum( logphi [ (self.runpars.N0 == n) ] )
            pnow = sum_logphi + lbeta + ljump  

            def accept():
                '''
                Updates the parameter values if proposed likelihood is 
                better than current likelihood
                '''
                r = np.exp( pnew -  pnow ) #acceptance criterion
                z = np.random.uniform( 1, 0, size = 1 )
        
                if z < r: #if accept with probability r, update psi and phi
                    self.runpars.Psi[ n ] = Psi_s
                    self.runpars.Phi = Phi_s #we update all cos only the relevant ones were changed by the phi_update()
                    
            accept()
        likelihoods()

                
    def gibbs(self):
        g = 0
        for gg in range(self.mcmcpars.ngibbs):
            #updating observation model parameters
            self.cutoff_mu_update( q = 0, distb = 0 ) #updates l-m cutoff mean for trapping data
            self.cutoff_mu_update( q = 0, distb = 1 ) #updates m-h cutoff mean for trapping data
            self.cutoff_mu_update( q = 1, distb = 0 ) #updates l-m cutoff mean for chewcard data
            self.cutoff_mu_update( q = 1, distb = 1 ) #updates m-h cutoff mean for chewchard data
            self.cutoff_sig_update( q = 0, distb = 0 ) #updates l-m cutoff mean for trapping data
            self.cutoff_sig_update( q = 0, distb = 1 ) #updates m-h cutoff mean for trapping data
            self.cutoff_sig_update( q = 1, distb = 0 ) #updates l-m cutoff mean for chewcard data
            self.cutoff_sig_update( q = 1, distb = 1 ) #updates m-h cutoff mean for chewchard data
            self.N_update()            

            #updating process model parameters    
            self.ab_update( coefs = self.runpars.a, betas = 0 ) #updates alphas of psi model, Nt0 = 0
            self.ab_update( coefs = self.runpars.b, betas = 1 ) #updates betas of tau model, Nt0 = 0
            for n in range(1,3): #where n is abundance state (1,2)
                self.tau_update(n = n)
                self.psi_update(n = n)
                
            if gg in self.mcmcpars.keep:
                #observation submodel parameters
                self.mu_trap_gibbs[g] = self.runpars.mu_trap
                self.sig_trap_gibbs[g] = self.runpars.sig_trap
                self.mu_chew_gibbs[g] = self.runpars.mu_chew
                self.sig_chew_gibbs[g] = self.runpars.sig_chew
                self.Nit_gibbs[g] = self.runpars.Nit
                
                #process submodel parameters
                self.b_gibbs[g] = self.runpars.b
                self.a_gibbs[g] = self.runpars.a
                self.tau_lh_gibbs[g] = self.runpars.Tau_lh
                self.psi_l_gibbs[g] = self.runpars.Psi_l
                self.tau_gibbs[g] = self.runpars.Tau
                self.psi_gibbs[g] = self.runpars.Psi

                g += 1
            #
            
class Results(object):
    '''
    Saves posterior estimates for each 
    MCMC run
    '''
    def __init__( self, mcmcobj ):
        self.b_gibbs = mcmcobj.b_gibbs
        self.a_gibbs = mcmcobj.a_gibbs
        self.tau_lh_gibbs = mcmcobj.tau_lh_gibbs
        self.tau_gibbs = mcmcobj.tau_gibbs
        self.psi_l_gibbs = mcmcobj.psi_l_gibbs
        self.psi_gibbs = mcmcobj.psi_gibbs
        #observation parameters
        self.mu_trap_gibbs = mcmcobj.mu_trap_gibbs
        self.sig_trap_gibbs = mcmcobj.sig_trap_gibbs
        self.mu_chew_gibbs = mcmcobj.mu_chew_gibbs
        self.sig_chew_gibbs = mcmcobj.sig_chew_gibbs
        self.Nit_gibbs = mcmcobj.Nit_gibbs

class PerfResults( object ):
    '''
    Saves mean, median and 95% CIs for each parameter based on overall MCMC run
    '''
    def __init__( self, results, runpars ):
        
        pars = 16 #number of parameters of interest
        self.means = np.zeros( pars )
        self.coverage = np.zeros( pars )
        self.scenarioID = runpars.data.scenarioID
        self.trialNum = runpars.data.trialNum        
        def PostMeans():
            self.means[ 0:2 ] = np.apply_along_axis( np.mean, 0, results.a_gibbs )
            self.means[ 2:4 ] = np.apply_along_axis( np.mean, 0, results.b_gibbs )
            self.means[ 4:6 ] = np.apply_along_axis( np.mean, 0, results.tau_gibbs[:,1:] )
            self.means[ 6:8 ] = np.apply_along_axis( np.mean, 0, results.psi_gibbs[:,1:] )
            self.means[ 8:10 ] = np.apply_along_axis( np.mean, 0, results.mu_trap_gibbs )
            self.means[ 10:12 ] = np.apply_along_axis( np.mean, 0, results.sig_trap_gibbs )
            self.means[ 12:14 ] = np.apply_along_axis( np.mean, 0, results.mu_chew_gibbs )
            self.means[ 14:16 ] = np.apply_along_axis( np.mean, 0, results.sig_chew_gibbs )
            self.means = np.round( self.means, decimals = 3 )

        def Coverage():
            self.resultTable = np.zeros( shape = (3, pars) )
            self.resultTable[ 0:3, 0:2 ] = np.apply_along_axis( basic.quantiles, 0, results.a_gibbs )
            self.resultTable[ 0:3, 2:4 ] = np.apply_along_axis( basic.quantiles, 0, results.b_gibbs )
            self.resultTable[ 0:3, 4:6 ] = np.apply_along_axis( basic.quantiles, 0, results.tau_gibbs[:,1:] )
            self.resultTable[ 0:3, 6:8 ] = np.apply_along_axis( basic.quantiles, 0, results.psi_gibbs[:,1:] )
            self.resultTable[ 0:3, 8:10 ] = np.apply_along_axis( basic.quantiles, 0, results.mu_trap_gibbs )
            self.resultTable[ 0:3, 10:12 ] = np.apply_along_axis( basic.quantiles, 0, results.sig_trap_gibbs )
            self.resultTable[ 0:3, 12:14 ] = np.apply_along_axis( basic.quantiles, 0, results.mu_chew_gibbs )
            self.resultTable[ 0:3, 14:16 ] = np.apply_along_axis( basic.quantiles, 0, results.sig_chew_gibbs )
       
            alpha =  runpars.data.alpha_l 
            beta = runpars.data.beta_lh 
            tau_mh = runpars.data.tau_mh
            tau_hl = runpars.data.tau_hl 
            psi_m = runpars.data.psi_m 
            psi_h = runpars.data.psi_h 
            mu_trap = runpars.data.mu_trap
            mu_chew = runpars.data.mu_chew
            sig_trap = runpars.data.sig_trap
            sig_chew = runpars.data.sig_chew 
       
            self.real = np.hstack( [ alpha, beta, tau_mh, tau_hl, psi_m, psi_h, mu_trap, sig_trap, mu_chew, sig_chew ] )
            for i in range( pars ):
                if self.real[i] >= self.resultTable[0,i] and self.real[i] <= self.resultTable[2,i]:
                    self.coverage[i] = 1

        PostMeans()
        Coverage()
                       
########            Main function
#######
def main( pklresults, pklrunpars, pklperfresults ):
    '''
    Run MCMC and pickle updated values from RunPars() and 
    results of MCMC() from Results()
    
    '''
    
    ##to determine if initial values are to be used or updated values from pickle:      
    ##start##
    if os.path.exists(pklrunpars):
        # read in pickled updated parameters from previous run stored in RunPars()
        fileobj = open(pklrunpars,'rb') #rb because you open it for reading it
        runpars = pickle.load(fileobj)
        fileobj.close()
    else:
        runpars = RunPars()# perfpars )  
        print('no updated run values provided so mcmc run started with initial values')        
    ##end###
    ##to run MCMC and store results in a pickle file:
    ##start##
    mcmcobj = MCMC( runpars ) #assigns an instance of MCMC class
    results = Results( mcmcobj ) #stores predicted results
    perfresults = PerfResults( results, runpars ) #stores mean posterior estimates & covarage
    #if we want to modify some of the values then:
        
    #path where pickled results will be stored:
    pklfile = open(pklresults, 'wb') #tells python to open file for writing in binary
    pickle.dump(results, pklfile) #tells pickle to save the mcmc object to the pklfile
    pklfile.close() #close it

    pklupdate = open(pklrunpars, 'wb')
    pickle.dump(runpars, pklupdate)
    pklupdate.close()

    pkl_file = open(pklperfresults, 'wb') #tells python to open file for writing in binary
    pickle.dump(perfresults, pkl_file) #tells pickle to save the mcmc object to the pkl_file
    pkl_file.close() #close it
    
    ##end###
    
if __name__ == '__main__':
        main()

################  end of script  #######################