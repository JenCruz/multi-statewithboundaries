#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
"""
Created by Dr Jennyffer Cruz
This script is designed to send jobs to the cluster
by calling other python scripts and pickles if restarting an mcmc run
"""
#importing required python scripts
import mcmc #script that runs mcmc
import optparse
from submit import PerfTest

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--runparsfile", dest="runparsfile", help="Input pickled runpars file")
        p.add_option("--resultsfile", dest="resultsfile", help="pickled file for all results")
        p.add_option("--performancefile", dest="performancefile", help="results from performance test pickle")
        p.add_option("--perfparsfile", dest="perfparsfile", help="parameter values for performance test pickle")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)
                                    

cmdargs = CmdArgs()
print(cmdargs.runparsfile)
print(cmdargs.resultsfile)
print(cmdargs.performancefile)

submit.main( pklperfpars = cmdargs.perfparsfile )
mcmc.main( pklresults = cmdargs.resultsfile, pklrunpars = cmdargs.runparsfile , pklperfresults = cmdargs.performancefile ) 
           
##################### end of script  ###########################