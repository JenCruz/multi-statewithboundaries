#!/usr/bin/env python

'''

Created by Dr Jennyffer Cruz
This script processes output from parallel processing 
for testing model performance by simulation of datasets 
under different scenarios.

'''

#importing required modules and python files
import numpy as np
import pickle
import prettytable
import os
import sys
from submit import PerfTest

class ProcessResults(object):
    '''
    This class creates mean and coverage tables from performance analysis
    '''
    def __init__( self ): #, pklperfresults ):

        outpath = os.getenv('MICE') #replace MICE with project folder location
        outperfpars = os.path.join( outpath, 'allperfresults/perfpars.pkl')
        fileobj = open(outperfpars, 'rb' )
        self.perfpars = pickle.load(fileobj)
        fileobj.close()
        
        #names of parameters that are evaluated
        self.names = np.array( [ 'a0', 'a1','b0', 'b1', 'tau_mh', 'tau_hl', 'psi_m', 'psi_h',
                 'mu0_trap', 'mu1_trap', 'sig0_trap', 'sig1_trap','mu0_chew', 
                 'mu1_chew', 'sig0_chew', 'sig1_chew' ] )

        self.covertable = np.zeros ( ( self.perfpars.trials, self.perfpars.no_scen,
                self.names.shape[0] ) ) 
        self.meantable = np.zeros ( ( self.perfpars.trials, self.perfpars.no_scen, 
                self.names.shape[0] ) ) 
                
        print('no_scen', self.perfpars.no_scen, 'trials', self.perfpars.trials )
        print( 'covertable', self.covertable )
        print( 'scenarios table', self.perfpars.table )
        np.savetxt("scenarios.txt", self.perfpars.table, fmt = '%.3e',  delimiter=",")
                        

    def FillTables( self, fname ):
        '''
        function that populates 3D tables of posterior means and 
        model coverage (whether the true parameter value fall within 95%CIs of 
        posterior distribution), which are the outcomes of the
        performance analysis using perfresults pickled files, 
        which are called from the command line using sys.arg[1:] 
        '''
        #load perfresults pickle file
        pklfile = open( fname, 'rb' )
        trialresults = pickle.load( pklfile )
        pklfile.close()
        
        print( 'real', trialresults.real )
        
        print( 'trialNum', trialresults.trialNum, 'scenarioID', trialresults.scenarioID )
        print( 'coverage', trialresults.coverage )
        print( 'means', trialresults.means )
        print( 'covertable slice', self.covertable[ trialresults.scenarioID, :, :] )
                
        #populate corresponding slice of tables:
        self.covertable[ trialresults.trialNum, trialresults.scenarioID, : ] = trialresults.coverage
        self.meantable[ trialresults.trialNum, trialresults.scenarioID, : ] = trialresults.means
        
        
    def SummTables( self, table ):
        '''
        This function summarises the 3D tables created in FillTables 
        as 2D tables 
        table is the table that we want summarised: 0 for meantable or 1 for covertable
        '''
        aa_names = self.names.tolist()
        aa_names.insert( 0, 'scenario_ID' )
        aa = prettytable.PrettyTable( aa_names )

        if table == 1:
            resultTable = np.mean( self.meantable[:, : , : ], axis = 0 )
            print('resultTable', resultTable)

        else:
            resultTable = np.sum( self.covertable[ :, :, : ], axis = 0 )
            print('resultTable', resultTable)
            
        for i in range(np.shape(resultTable)[0]):
            scen = [i]
            row = scen + resultTable[i].tolist()
            aa.add_row(row)

        print(aa)


########            Main function
#######
def main():

    perftables = ProcessResults()
                                                
if __name__ == '__main__':
    main()
                                                    
######################## end of script  ######################