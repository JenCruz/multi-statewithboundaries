#!/usr/bin/env python

"""
Created by Dr Jennyffer Cruz 
This script submits multiple jobs in parallel, creating unique jobnames for each
to test performance of the multi-state, multi-season occupancy model under alternative scenarios 
The script also creates a table with parameter values for each scenario and determines how many scenarios and 
how many datasets per scenario are created.
The multi-state, multi-season model uses continuous indices of abundance, expert opinion and environmental factors
"""

import numpy as np
import os
import pickle

class PerfTest( object ) :
    '''
    Performance test scenarios
    no_scen = number of scenarios assessed in the performance test
    scen_table = table of scenarios (rows) by parameters of interest (columns)
    '''
    def __init__( self, no_scen, trials ) :
        self.no_scen = no_scen #number of scenarios
        self.trials = trials #no of trials per scenario
        k = np.repeat( 5, self.no_scen )
        k[1] = 2
        k[2] = 10
        sigma_trap = np.repeat( 0.02, self.no_scen )
        sigma_chew = np.repeat( 0.02, self.no_scen )
        sigma_trap[3] = 0.05
        sigma_chew[3] = 0.05
        sigma_trap[4] = 0.1
        sigma_chew[4] = 0.1
        sigma_chew[5] = 0.05
        sigma_chew[6] = 0.1
        mu_lm_trap = np.repeat( 0.33, self.no_scen )
        mu_mh_trap = np.repeat( 0.66, self.no_scen )
        mu_lm_chew = np.repeat( 0.33, self.no_scen )
        mu_mh_chew = np.repeat( 0.66, self.no_scen )
        mu_lm_trap[7] = 0.15
        mu_mh_trap[7] = 0.3
        mu_lm_chew[7] = 0.05
        mu_mh_chew[7] = 0.4
        M_lm_trap = mu_lm_trap.copy()
        M_mh_trap = mu_mh_trap.copy()
        M_lm_chew = mu_lm_chew.copy()
        M_mh_chew = mu_mh_chew.copy()
        M_lm_trap[8] = 0.1
        M_mh_trap[8] = 0.8
        M_lm_chew[8] = 0.4
        M_mh_chew[8] = 0.6
        missing = np.zeros( self.no_scen )
        missing[9] = 20
        missing[10] = 40
        scen_table = np.vstack( [ k, sigma_trap, sigma_chew, mu_lm_trap,
            mu_mh_trap, mu_lm_chew, mu_mh_chew, M_lm_trap,
            M_mh_trap, M_lm_chew, M_mh_chew, missing ] )
        self.table = np.transpose( scen_table )

########            Main function
#######
def main():
    
    perfpars = PerfTest( no_scen = 11, trials = 900 )

    #pickle perfparams:
    outpath = os.getenv('MICE') #replace with relevant folder where you want to save work 
    pklperfpars = os.path.join(outpath, 'perfpars.pkl') #joins projects path to the filename
    pklfile = open(pklperfpars, 'wb') #tells python to open file for writing in binary
    pickle.dump(perfpars, pklfile) #tells pickle to save the mcmc object to the pklfile
    pklfile.close() #closes it

    #loop to submit x number of trials per x scenarios and create unique job names for each job
    for scenarioID in range( perfpars.no_scen ):
        for trialNum in range( perfpars.trials ):
            jobName = '%d_%d' % (scenarioID, trialNum)
            cmd = 'sbatch -J %s parallel.sl' % jobName
            print( cmd )
            os.system(cmd)

    ###
    
if __name__ == '__main__':
        main()

### end of script ########