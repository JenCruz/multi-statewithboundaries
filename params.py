
#!/usr/bin/env python
#
"""
Created by Dr Jennyffer Cruz
This script contains classes for all the parameter values needed to run the
multi-state multi-season occupancy model 
Classes: 
Priors: priors 
Params: initial coefficient estimates of the model  
MCMCParams: mcmc parameters including no. of runs, thinning and burnin rates
"""

#importing required modules and python scripts
import basic 
import numpy as np
from scipy import stats
import simdata

#defining initial parameter estimates
class Pars(object):
    '''
    Initial coefficient estimates of the model.
    process submodel attributes:
    b = initial betas for tau_lh logit function
    a = iniital alphas for psi_l logit function
    psi = initial estimates for psis without logit functions
    tau = inital estimates for taus without logit functions
    phi = inital estimates of phi
    cmat = jump distribution for the metropolis step
    tjump & pjump = jump distributions with metropolis-hastings step for tau_lh & psi_l
    observation submodel attributes:
    a, b = parameters of beta cutoff distributions
    mu = mean of beta cutoff distributions
    sig = var of beta cutoff distributions
    prZ = prob of data given N = 0,1,2 across observation methods
    
    '''
    def __init__(self):
        #simulate data
        self.data = simdata.SimData()
        #get priors
        priors = Priors()
            
        #### process submodel parameters ####
        #########################################
        #initial coef estimates for transition prob models
        b_lh = np.array( [-1.0, 2.0] ) #transition from low to med
        a_l = np.array( [1.0, -2.0] ) #transition from med to high
        self.b =  b_lh
        self.a = a_l 
        logit_Tau_lh = np.dot( self.data.x, b_lh ) #logit of transition from low to high 
        logit_Psi_l = np.dot( self.data.x, a_l ) #logit of prob of remaining low
        self.Tau_lh = basic.inv_logit( logit_Tau_lh ) #prob of transitioning from low to high (mouse plague)
        self.Psi_l = basic.inv_logit( logit_Psi_l ) #prob of remaining low
        Tau_mh = 0.1 #prob of transitioning from med to high
        Tau_hl = 0.4 #mean transition prob high-low (population crash)
        self.Tau = np.array( [ 0.0, Tau_mh, Tau_hl ] ) #set up so that we can easily change which 
        #taus have functions associated with them
        Psi_m = 0.2 #prob of staying in med 
        Psi_h = 0.2 #prob of staying in a high abundance state
        self.Psi = np.array( [ 0.0, Psi_m, Psi_h ] ) #0 for Psi_l cos there is a function model for it
        #so it won't be used in the mcmc

        #jump distribution for bupdate with metropolis jump
        self.ccmat =  np.array( [ [1.0, 0.0 ], [ 0.0, 1.0 ] ] )#assumes no correlation
        self.cmat = np.multiply( 0.5, self.ccmat )
        #jump distribution for tau update with metropolis-hastings needs to be much smaller than the tau values  
        self.Tjump = np.array( [0.05] ) #variance for updating tau_hl 
        #jump distribution for phi update with metropolis-hastings
        self.Pjump = np.array( [0.05] ) #variance for updating all phi parameters
        
        #### observation submodel parameters #######
        #######################################################
        #trapping data initial estimates
        self.mu_trap = priors.Mprior_trap.copy() #mean of cut-off distributions for trapping
        self.sig_trap = priors.epsprior_trap.copy() #sd of errors for trapping
        #chew card data initial estimates
        self.mu_chew = priors.Mprior_chew.copy() #mean of cut-off distributions for trapping
        self.sig_chew = priors.epsprior_chew.copy() #sd of errors for trapping
        
        #calculating prob that data falls in a given abundance state, 
        #given cut off distributions:    
        self.prZ_trap = self.get_prZs( self.data.z_trap, self.mu_trap, self.sig_trap )
        self.prZ_chew = self.get_prZs( self.data.z_chew, self.mu_chew, self.sig_chew )

        #getting random initial abundance states
        #Nit is 1D array of size sites*T
        self.Nit = np.random.randint( 0, 3, (self.data.sites*self.data.T) ) #draw random numbers from either 0, 1, or 2
        self.N0 = self.Nit[ :(-self.data.sites) ] #1D array of Nt        
        self.N1 = self.Nit[ self.data.sites: ] #1D array of Nt+1
        self.Phi = self.get_Phi()    

        #jump variance for updating ab parameters of cutoff beta distribuions:
        self.mujump = np.array( [0.1] )#jump distribution for the cutoff mean
        self.sigjump = np.array( [0.05] ) #jump distribution for the cutoff var

    def get_prZs( self, z, mu, sig ):
        '''
        This function generates probability of data
        using two boundary distributions
        attributes:
        z = monitoring data 
        mu = mean of boundary distribution
        sig = error of boundary distribution
        '''
        logitmu = basic.logit(mu)
        cdf01 = stats.norm.cdf( basic.logit(z), loc = logitmu[0], scale = sig[0] )
        cdf12 = stats.norm.cdf( basic.logit(z), loc = logitmu[1], scale = sig[1] ) 
        prZN0 = ( 1 - cdf01 ) * ( 1 - cdf12 )
        prZN1 = cdf01 * ( 1 - cdf12 )
        prZN2 = cdf01 * cdf12
        pvals = np.vstack( [prZN0, prZN1, prZN2] )
        pvals = np.transpose( pvals )
        pvals[pvals < 1.0e-10 ] = 1.0e-10
        return pvals

    def obv_state( self, prZ ):
        '''
        This function generates observed states
        depending on all data collected for each 
        site each season
        prZ = combined prob that data (across datasets) 
            falls within each abundance state
        obvN = predicted observed abundance at each site
        each season as a function of all surveys 
        (from q methods and k times)
        '''
        obvN= np.zeros( ( self.data.sites * self.data.T, 3 ), dtype = np.int8 )
        for i in range( len(obvN) ):
            obvN[i,] = np.random.multinomial( n = 1, pvals =  prZ[i,] )
        return obvN

        
    def get_Phi( self ):  
        '''
        Calculate initial phi based on observed abundance values
        attributes:
        n = initial abundance state either: low(0), med(1), or high(2)
        '''
        Phi =  np.zeros( ( self.data.sites * (self.data.T - 1), 3, 3 ) )
        Phi[:,0,1] = ( 1 - self.Psi_l ) * (1 - self.Tau_lh )
        Phi[:,0,2] = ( 1 - self.Psi_l ) * self.Tau_lh
        Phi[:,0,0] = self.Psi_l
        Phi[:,1,0] = ( 1 - self.Psi[1] ) * ( 1 - self.Tau[1] )
        Phi[:,1,1] = self.Psi[1]
        Phi[:,1,2] =  ( 1 - self.Psi[1] ) * self.Tau[1]
        Phi[:,2,0] = ( 1 - self.Psi[2] ) * self.Tau[2]
        Phi[:,2,1] = ( 1 - self.Psi[2] ) * ( 1 - self.Tau[2] )
        Phi[:,2,2] = self.Psi[2]
        return Phi

    
#defining priors
class Priors( object ):
    '''Vague priors.
    attributes: 
    bprior = beta priors, vmat = covariance matrix.
    '''
    def __init__( self ):

#        jobName = os.getenv('SLURM_JOB_NAME')
#        scenarioID, trialNum = jobName.split('_')
#        scenarioID = int(scenarioID)
#        
#        outpath = os.getenv('MICE')
#        outperfpars = os.path.join( outpath, perfpars.pkl)
#        fileobj = open(outperfpars, 'rb' )
#        perfpars = pickle.load(fileobj)
#        fileobj.close()

        data = simdata.SimData()

        ###### process submodel priors #########
        #vague priors for parameters of tau_lh and psi_l logit functions
        self.bprior = np.array( [-3.0, 2.0] ) #normal prior for b parameters in tau_lh
        self.aprior = np.array( [2.0, -4.0] ) #normal prior for a parameters in psi_l
        self.vprior = np.diagflat( np.tile( 5.0, 2.0 ) ) #variance prior for tau_lh & psi_l functions
        
        #prob of staying in med or high state is low so:
        psiprior_m = np.array( [1.0, 3.0] ) #beta prior with prob skewed to the left
        psiprior_h = np.array( [1.0, 3.0] ) #beta prior with prob skewed to the left
        psiprior_l = np.array( [0.0, 0.0] ) #this one won't be used 
        self.psi_prior =np.vstack( ( psiprior_l, psiprior_m, psiprior_h ) ) #note we don't use the psiprior_l
        #parameters a and b suggesting a distribution skewed to the right
        tauprior_lh = np.array( [0.0, 0.0 ] ) #this one won't be used since tau_lh has a logit function
        tauprior_mh = np.array( [1.0, 3.0 ] )
        tauprior_hl = np.array( [3.0, 1.0] ) #beta prob with mean at 0.5 
        self.tau_prior = np.vstack( ( tauprior_lh, tauprior_mh, tauprior_hl ) )
        
        ##### #observation submodel priors  ########
        #trapping data priors:
        self.Mprior_trap = np.hstack( [ data.perfpars.table[ data.scenarioID, 7 ], data.perfpars.table[ data.scenarioID, 8 ] ] )# data.mu_trap.copy() #making prior same as original
        logitM_trap = basic.logit( self.Mprior_trap )
        self.epsprior_trap = data.sig_trap.copy() * 5 #making SD 4 times bigger than original
        logit_muprior_trap = np.vstack( ( logitM_trap, self.epsprior_trap ) )
        self.logit_muprior_trap = np.transpose( logit_muprior_trap ) #normal prior for mean of normal cutoff distribution

        scale_trap = np.array( [0.3, 0.3] ) #scale parameter of gamma prior distribution
        shape_trap = np.array( [1.1, 1.1] ) #scale parameter of gamma prior distribution
        gammprior_trap = np.vstack( ( shape_trap, scale_trap ) )
        self.gammprior_trap = np.transpose( gammprior_trap ) #gamma prior (shape and scale)
                
        #chewcard data priors:
        self.Mprior_chew = np.hstack( [ data.perfpars.table[ data.scenarioID, 9 ], data.perfpars.table[ data.scenarioID, 10 ] ] ) #data.mu_chew.copy() #
        logitM_chew = basic.logit( self.Mprior_chew )
        self.epsprior_chew =  data.sig_chew.copy() * 5

        logit_muprior_chew = np.vstack( ( logitM_chew, self.epsprior_chew ) )
        self.logit_muprior_chew = np.transpose( logit_muprior_chew ) #normal prior for mean of normal cutoff distribution

        scale_chew = np.array( [0.3, 0.3] ) #scale parameter of gamma prior distribution
        shape_chew = np.array( [1.1, 1.1] ) #scale parameter of gamma prior distribution
        gammprior_chew = np.vstack( ( shape_chew, scale_chew ) )
        self.gammprior_chew = np.transpose( gammprior_chew ) 


#defining mcmc run parameters
class MCMCPars(object):
    '''
    Values to determine the mcmc run.
    attributes: 
    ngibbs = number of iterations to run, thin = thin rate, 
    burnin = burnin rate
    '''
    def __init__( self, ngibbs = 5000, thin = 15, burnin = 1000 ):
        self.ngibbs = ngibbs  #number of runs
        self.thin = thin       # thin rate
        self.burnin = burnin  # burn in number of iterations
        self.keep = np.arange( start = self.burnin, stop = (
            self.ngibbs + self.thin), step = self.thin )
        


########            Main function
#######
def main():

if __name__ == '__main__':
        main()

 
###############  end of script ###################